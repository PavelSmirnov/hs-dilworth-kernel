import os
from setuptools import find_packages, setup
from setuptools.extension import Extension
import sys
import site

site.ENABLE_USER_SITE = True
from Cython.Build import cythonize

SYSTEM = sys.platform
PACKAGE_ROOT = os.path.abspath(os.path.dirname(__file__))
SRC_DIR = os.path.join(PACKAGE_ROOT, 'src')

def iterate_all_files(dir_name):
    for subdir, _, files in os.walk(dir_name):
        for file_name in files:
            yield os.path.join(SRC_DIR, subdir, file_name)

CPP_SRCS = [path for path in iterate_all_files(SRC_DIR) if path.endswith('.cpp')]

REQUIREMENTS = [
    "pytest>=5.3.1",
    "setuptools>=49.2.0",
    "Cython>=0.29.21",
    "PuLP>=2.3",
]

include_dirs = [SRC_DIR]
library_dirs = []
libraries = []
extra_objects = []
extra_compile_args = ['-std=c++17', '-O3']

# GUROBI
gurobi_path = os.environ.get('GUROBI_PATH')
if gurobi_path is not None:
    include_dirs += [f"{gurobi_path}/include"]
    library_dirs += [f"{gurobi_path}/lib"]
    libraries += ["gurobi90"]
    if SYSTEM in ['linux', 'linux2']:
        libraries += ["gurobi_g++5.2"]
    else:
        libraries += ["gurobi_c++"]
    extra_compile_args += ["-DUSE_GUROBI"]

# OPENCL
use_opencl = os.environ.get('USE_OPENCL')
if use_opencl is not None and use_opencl in ['1', 'true', 'True']:
    libraries += ['OpenCL']
    extra_compile_args += ["-DUSE_OPENCL"]

cython_ext = Extension('fastweihe.cython.cython',
                       [os.path.join('fastweihe', 'cython', 'cython.pyx')] + CPP_SRCS,
                       language='c++',
                       include_dirs=include_dirs,
                       library_dirs=library_dirs,
                       libraries=libraries,
                       extra_objects=extra_objects,
                       extra_compile_args=extra_compile_args)

setup(name='fastweihe',
      py_modules=['fastweihe'],
      packages=find_packages(),
      ext_modules=cythonize([cython_ext], compiler_directives={'language_level': 3}),
      install_requires=REQUIREMENTS)
