import abc
import os
import sys

import pulp


HAVE_GUROBI = False
GUROBI_PATH = os.environ.get('GUROBI_PATH')
if GUROBI_PATH is not None:
    print('Using GUROBI as LP solver')
    HAVE_GUROBI = True
else:
    print('Using CBC as LP solver')


def _get_ilp_solver():
    if HAVE_GUROBI:
        return pulp.GUROBI_CMD(path=os.path.join(GUROBI_PATH, 'bin', 'gurobi_cl'), msg=False)
    return pulp.PULP_CBC_CMD(msg=0)


class PythonLowerBoundEstimator(abc.ABC):
    def __init__(self, **kwargs):
        pass

    @abc.abstractmethod
    def __call__(self, input, **kwargs):
        raise NotImplementedError


class ILPLowerBoundEstimator(PythonLowerBoundEstimator):
    def __call__(self, input, **kwargs):
        problem = pulp.LpProblem("hitting_set", pulp.LpMinimize)

        vertex_variables = {}
        for edge, demand in zip(input.edges, input.edge_demands):
            if not edge: # may be incorrect on infeasible inputs
                continue

            hit_count = 0
            for vertex in edge:
                if vertex not in vertex_variables:
                    vertex_variables[vertex] = pulp.LpVariable(f"x{vertex}", 0, 1, cat=self._get_variable_category())
                hit_count += vertex_variables[vertex]
            problem += hit_count >= demand

        if not vertex_variables: # may be incorrect on infeasible inputs
            return 0

        objective = 0
        for _, variable in vertex_variables.items():
            objective += variable
        problem.setObjective(objective)

        status = problem.solve(solver=_get_ilp_solver())
        if pulp.LpStatus[status] != 'Optimal':
            print(f"LP problem solution status is: {status}.", file=sys.stdout)

        return pulp.value(objective)

    @abc.abstractmethod
    def _get_variable_category(self):
        raise NotImplementedError


class LinearProblemLowerBoundEstimator(ILPLowerBoundEstimator):
    def _get_variable_category(self):
        return 'Continuous'


class ExactAnswerEstimator(ILPLowerBoundEstimator):
    def _get_variable_category(self):
        return 'Binary'
