from collections import Counter
import os

from fastweihe.cython import HypergraphData


class Hypergraph(object):
    def __init__(self, edges=None, edge_demands=None, path=None, data=None, fit_demands=False):
        """
        Parameters
        ----------
        edges: list[list[int]]
            The hypergraph edges.
            Only one of 'edges' and 'path' parameter must be passed.

        edge_demands: list[int] (default: [1] * len(edges))
            The hypergraph edges demands.
            Only one of 'edge_demands' or 'path' must be passed.

        path: str
            A path to the hypergraph (see '.hyp' file format).
            If set, none of 'edges' or 'edge_demands' parameters should be passed.

        data: HypergraphData
            To pass hypergraph data directly.

        fit_demands: bool
            Whether to set edge demand to the edge's size
            for edges with demand greater than size.
            (Parameter is ignored if data parameter is passed.)
        ----------
        '.hyp' text file format:
            The first line is a header (must be "classic" or "weighted").
            Each of the next lines represents an edge.

            If the header is "classic", an edge is represented with its vertex ids (starting with 0) separated by spaces.
            Example:
                classic
                0 1 2
                1 2 3 4
                2 3
            This file describes a hypergraph with edges (0, 1, 2), (1, 2, 3, 4) and (2, 3).
            By default all of the edges have demand 1.

            If the header is "weighted", a classic edge representation is preceded with an edge demand.
            Example:
                weighted
                2 0 1 2
                3 1 2 3 4
                1 2 3
            Now the edge (0, 1, 2) has demand 2, the edge (1, 2, 3, 4) has demand 3 and the edge (2, 3) has demand 1.
        """
        self._data = None
        if data is not None:
            assert isinstance(data, HypergraphData)
            assert edges is None and edge_demands is None and path is None
            self._data = data
            return
        self._data = HypergraphData()
        if path is not None:
            assert isinstance(path, str), "Path must be a str."
            assert edges is None, "Edges provided, but a hypergraph is read from file."
            assert edge_demands is None, "Edge demands provided, but a hypergraph is read from file."
            edges, edge_demands = self._read_edges_from_file(path)
        elif edges is not None:
            # TODO: maybe edges and edge_demands can be array like?
            assert (isinstance(edges, list) and
                    all(isinstance(edge, list) for edge in edges) and
                    all(all(isinstance(vertex, int) for vertex in edge) for edge in edges)), "Edges must be a list[list[int]]."
        else:
            assert False, "Either edges or path must be provided to initilize a Hypergraph."
        edges = [list(sorted(set(edge))) for edge in edges] # no duplicates allowed
        if edge_demands is None:
            edge_demands = [1] * len(edges)
        else:
            if isinstance(edge_demands, int):
                assert edge_demands >= 0, "Edge demands must be non-negative."
                edge_demands = [edge_demands] * len(edges)
            assert (isinstance(edge_demands, list) and
                    all(isinstance(demand, int) for demand in edge_demands)), "Edge demands must be a list[int]."
            assert len(edges) == len(edge_demands), f"The number of edge demands {len(edge_demands)} must be equal to the number of edges {len(edges)}."
        if fit_demands:
            edge_demands = [min(demand, len(edge)) for demand, edge in zip(edge_demands, edges)]
        self._data.set_edges(edges, edge_demands)

    @property
    def edges(self):
        return self._data.edges

    @property
    def vertices(self):
        return self._data.vertices

    @property
    def edge_demands(self):
        return self._data.edge_demands

    @property
    def num_edges(self):
        return self._data.num_edges

    @property
    def num_vertices(self):
        return self._data.num_vertices

    @property
    def num_used_vertices(self):
        return self._data.num_used_vertices

    @property
    def data(self):
        return self._data

    @property
    def is_consistent(self):
        return self._data.is_consistent

    def _read_edges_from_file(self, path):
        assert os.path.isfile(path), f"No such file: '{path}'."
        with open(path, 'r') as f:
            header = next(f).strip()
            if header == 'classic':
                return [[int(vertex) for vertex in edge.split()] for edge in f], None
            elif header == 'weighted':
                edge_infos = [[int(value) for value in line.split()] for line in f]
                assert all(map(len, edge_infos)), f"All edges must have demands."
                edges = [values[1:] for values in edge_infos]
                edge_demands = [values[0] for values in edge_infos]
                return edges, edge_demands

    def __repr__(self):
        return f"edges: {repr(self._data.edges)}\ndemands: {repr(self._data.edge_demands)}"

    def __eq__(self, other):
        self_edges_set = Counter(map(frozenset, self.edges))
        other_edges_set = Counter(map(frozenset, other.edges))
        return self_edges_set == other_edges_set and set(self.edge_demands) == set(other.edge_demands)

    def save(self, path, save_demands=True):
        # TODO: this can be implemented in C++
        with open(path, 'w') as f:
            if not save_demands:
                f.write("classic\n")
                f.write("\n".join([' '.join(map(str, edge)) for edge in self.edges]))
            else:
                edge_demands = self.edge_demands
                f.write("weighted\n")
                f.write("\n".join([' '.join(map(str, [edge_demands[i]] + edge))
                                   for i, edge in enumerate(self.edges)]))
