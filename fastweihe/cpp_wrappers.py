from fastweihe.cython import LBEstimator, Reducer
from fastweihe.hypergraph import Hypergraph


class CppReducer(object):
    def __init__(self, name, **kwargs):
        self._reducer = Reducer(name)

    def __call__(self, input, log_level, **kwargs):
        output_data, in_hitting_set, is_reduced = self._reducer.reduce(input.data, log_level, **kwargs)
        return Hypergraph(data=output_data), in_hitting_set, is_reduced

    @property
    def is_available(self):
        return self._reducer.is_available


class CppLBEstimator(object):
    def __init__(self, name, **kwargs):
        self._estimator = LBEstimator(name)

    def __call__(self, input, log_level, **kwargs):
        return self._estimator.estimate(input.data, log_level, **kwargs)
