import abc

from fastweihe.cpp_wrappers import CppReducer
from fastweihe.cython import HittingSetData
from fastweihe.hypergraph import Hypergraph
from fastweihe.python_reducers import CbcReducer, FLowerBoundReducer, FWeiheGpuReducer, WeiheGpuReducer


class BaseHittingSetReducer(abc.ABC):
    def __init__(self, **kwargs):
        self._in_hitting_set = None
        self._is_reduced = None

    @abc.abstractmethod
    def __call__(self, input, **kwargs):
        """
        Reduces a Hitting Set problem instance.
        Returns a reduced hypergraph.

        Parameters:
        ___________
        hypergraph: fastweihe.Hypergraph
            An input hypergraph.
        """
        raise NotImplementedError

    @property
    def in_hitting_set(self):
        """
        Returns a list of vertices put into the hitting set on the last reduction.
        """
        if self._in_hitting_set is None:
            return None
        return self._in_hitting_set.vertices

    @property
    def is_reduced(self):
        """
        Returns boolean value whether the last algorithm run performed any reduction.
        """
        return self._is_reduced


class HittingSetReducer(BaseHittingSetReducer):
    _CPP_REDUCERS = ['identity', 'remove-full-edges',
                     'weihe-naive', 'weihe-fast',
                     'f-weihe-naive', 'f-weihe-fast',
                     'f-weihe-naive-worse', 'f-weihe-fast-worse',
                     'solve-gurobi', 'f-weihe-gpu-opencl',
                     ]
    _PYTHON_REDUCERS = {'f-lower-bound': FLowerBoundReducer,
                        'solve-cbc': CbcReducer,
                        'weihe-gpu': WeiheGpuReducer,
                        'f-weihe-gpu': FWeiheGpuReducer}

    def __init__(self, algorithm, have_cuda=False, **kwargs):
        """
        A class for Hitting Set problem reducers.

        Parameters:
        ___________
        algorithm: str
            The reducer algorithm.
            (See HittingSetReducer.get_available_algorithms.)

        have_cuda: bool (default: False)
            If True, a GPU will be used for 'weihe-gpu' and 'f-weihe-gpu'.
        """
        super(HittingSetReducer, self).__init__(**kwargs)
        self._name = algorithm
        self._reducer = self._choose_reducer(algorithm, have_cuda=have_cuda, **kwargs)

    def _choose_reducer(self, algorithm, have_cuda, **kwargs):
        if algorithm in self._CPP_REDUCERS:
            return CppReducer(algorithm, **kwargs)
        if algorithm in self._PYTHON_REDUCERS:
            reducer_cls = self._PYTHON_REDUCERS[algorithm]
            return reducer_cls(have_cuda=have_cuda, **kwargs)
        assert False, f"Unknown reducer '{algorithm}'."

    def __call__(self, input, log_level='error', **kwargs):
        assert isinstance(input, Hypergraph), "HittingSetReducer input must be a hypergraph."
        reduced_instance, self._in_hitting_set, self._is_reduced = self._reducer(input, log_level=log_level, **kwargs)
        return reduced_instance

    @classmethod
    def get_available_algorithms(cls):
        """
        Returns a list of available algorithms.
        Any of them can be passed to HittingSetReducer constructor.
        """
        return list(set(cls._CPP_REDUCERS).union(cls._PYTHON_REDUCERS.keys()))

    @property
    def is_available(self):
        return self._reducer.is_available

    def __repr__(self):
        return f"{self._name}"


class StackingHittingSetReducer(BaseHittingSetReducer):
    def __init__(self, reducers, **kwargs):
        """
        A Hitting Set problem reducer running through a stack of reducers.

        Parameters:
        ___________
        reducer: list of reducers or strs
            Each str converts to the corresponding reducer.
            (See HittingSetReducer.get_available_algorithms.)
        """
        super(StackingHittingSetReducer, self).__init__(**kwargs)

        self._reducers = []
        for reducer in reducers:
            reducer = compose_hitting_set_reducer(reducer, **kwargs)
            self._reducers.append(reducer)

    def __call__(self, input, **kwargs):
        self._in_hitting_set = HittingSetData()
        self._is_reduced = False
        for reducer in self._reducers:
            input = reducer(input, **kwargs)
            self._is_reduced = self._is_reduced or reducer.is_reduced
            self._in_hitting_set.add(reducer._in_hitting_set)
        return input

    def __repr__(self):
        names = ' -> '.join(map(repr, self._reducers))
        return f"[{names}]"


class CyclingHittingSetReducer(BaseHittingSetReducer):
    def __init__(self, reducer, num_iterations=None, **kwargs):
        """
        A Hitting Set problem cycling reducer.

        Parameters:
        ___________
        reducer: BaseHittingSetReducer or str
            str converts to the corresponding reducer.
            (See HittingSetReducer.get_available_algorithms.)

        num_iterations: int (default: None)
            Num
        """
        super(CyclingHittingSetReducer, self).__init__(**kwargs)
        self._reducer = compose_hitting_set_reducer(reducer, **kwargs)
        self._num_iterations = num_iterations

    def __call__(self, input, **kwargs):
        self._in_hitting_set = HittingSetData()
        self._is_reduced = False
        iteration = 0
        while self._num_iterations is None or iteration < self._num_iterations:
            iteration += 1
            input = self._reducer(input, **kwargs)
            if not self._reducer.is_reduced:
                return input
            self._in_hitting_set.add(self._reducer._in_hitting_set)
            self._is_reduced = True

    def __repr__(self):
        name = repr(self._reducer)
        times = '...' if self._num_iterations is None else self._num_iterations
        return f"({name} x {times})"


def compose_hitting_set_reducer(composition, **kwargs):
    if isinstance(composition, BaseHittingSetReducer):
        return composition
    if isinstance(composition, str):
        return HittingSetReducer(algorithm=composition, **kwargs)
    if isinstance(composition, list):
        return StackingHittingSetReducer([compose_hitting_set_reducer(reducer, **kwargs) for reducer in composition], **kwargs)
    if isinstance(composition, tuple) and len(composition) == 2:
        no_num_iterations_kwargs = {key : value for key, value in kwargs.items() if key != 'num_iterations'}
        return CyclingHittingSetReducer(compose_hitting_set_reducer(composition[0], **kwargs), num_iterations=composition[1], **kwargs)
    assert False, f"Can't parse Hitting Set reducer: {composition}"
