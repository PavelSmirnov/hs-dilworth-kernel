import abc
import math

import pulp

IS_TORCH_AVAILABLE = False
try:
    import torch
    IS_TORCH_AVAILABLE = True
except ImportError:
    pass

from fastweihe.cython import (construct_push_through_hypergraph_data, HittingSetData,
                             output_log, remove_edges_from_hypergraph)
from fastweihe.hypergraph import Hypergraph
from fastweihe.lb_estimator import compose_lower_bound_estimator


class PythonReducer(abc.ABC):
    def __init__(self, **kwargs):
        pass

    def __call__(self, input, **kwargs):
        result = self._reduce(input, **kwargs)
        if len(result) == 3:
            output, in_hitting_set, is_reduced = result
        elif len(result) == 4:
            edges, edge_demands, in_hitting_set, is_reduced = result
            output = Hypergraph(edges=edges, edge_demands=edge_demands)
        else:
            assert False
        if is_reduced:
            assert input != output, input
        return output, HittingSetData(in_hitting_set), is_reduced

    @abc.abstractmethod
    def _reduce(self, input, log_level, **kwargs):
        raise NotImplementedError

    @property
    def is_available(self):
        return True


def heat_up_gpu_torch():
    if IS_TORCH_AVAILABLE:
        WeiheGpuReducer(have_cuda=True)(Hypergraph([[0]]), log_level='silent')


class CbcReducer(PythonReducer):
    def _reduce(self, input, log_level, **kwargs):
        problem = pulp.LpProblem("hitting_set", pulp.LpMinimize)

        vertex_variables = {}
        for edge, demand in zip(input.edges, input.edge_demands):
            if not edge: # may be incorrect on infeasible inputs
                continue

            hit_count = 0
            for vertex in edge:
                if vertex not in vertex_variables:
                    vertex_variables[vertex] = pulp.LpVariable(f"x{vertex}", 0, 1, cat='Binary')
                hit_count += vertex_variables[vertex]
            problem += hit_count >= demand

        if not vertex_variables: # may be incorrect on infeasible inputs
            return Hypergraph([]), [], False

        objective = 0
        for _, variable in vertex_variables.items():
            objective += variable
        problem.setObjective(objective)

        status = problem.solve(solver=pulp.PULP_CBC_CMD(msg=0))
        if pulp.LpStatus[status] != 'Optimal':
            print(f"LP problem solution status is: {status}.", file=sys.stdout)

        hitting_set = [vertex for vertex, variable in vertex_variables.items() if variable.value() > 0.5]

        return Hypergraph([]), hitting_set, (input != Hypergraph([]))


class FLowerBoundReducer(PythonReducer):
    LB_EPS = 1e-6

    def __init__(self, lower_bound_estimator='trivial', **kwargs):
        super(FLowerBoundReducer, self).__init__(**kwargs)
        self._lb_estimator = compose_lower_bound_estimator(lower_bound_estimator, **kwargs)

    def _reduce(self, input, log_level, **kwargs):
        edge_demands = input.edge_demands
        removed_edges_in_order = []
        for i in range(len(edge_demands)):
            hypergraph = Hypergraph(data=construct_push_through_hypergraph_data(input, i, removed_edges_in_order))
            lower_bound = self._lb_estimator(hypergraph)
            demand = edge_demands[i]
            if math.ceil(lower_bound - self.LB_EPS) >= edge_demands[i]:
                removed_edges_in_order.append(i)
        is_reduced = len(removed_edges_in_order) > 0
        return Hypergraph(data=remove_edges_from_hypergraph(input, removed_edges_in_order)), [], is_reduced

    def __repr__(self):
        name = repr(super(FLowerBoundReducer, self))
        estimator = repr(self._lb_estimator)
        return f"{name}({estimator})"


def _float_tensor_is_zero(tensor):
    return torch.abs(tensor) < 0.5


def _float_tensor_greater_equal(l_tensor, r_tensor):
    return l_tensor >= r_tensor - 0.5


class WeiheGpuPythonReducer(PythonReducer):
    def __init__(self, have_cuda, max_num_iterations=None, **kwargs):
        super(WeiheGpuPythonReducer, self).__init__(**kwargs)
        self._max_num_iterations = max_num_iterations
        if IS_TORCH_AVAILABLE:
            self._device = torch.device('cuda') if have_cuda and torch.cuda.is_available() else torch.device('cpu')
        else:
            self._device = None

    @property
    def device(self):
        return self._device

    def _reduce(self, input, log_level, **kwargs):
        if not IS_TORCH_AVAILABLE:
            output_log(log_level, 'error', 'GPU algorithms are unavailable: no `torch` package found\n')
            return (input, )

        edges = input.edges
        num_edges = len(edges)
        num_vertices = len(input.vertices)
        edge_demands = torch.tensor(input.edge_demands).to(self.device)

        incidency_matrix = torch.zeros((num_edges, num_vertices), dtype=bool).to(self.device)
        for e, edge in enumerate(edges):
            incidency_matrix[e, edge] = True
        # TODO: remove edge_ids
        edge_ids = torch.arange(0, num_edges, dtype=int).to(self.device)
        vertex_ids = torch.arange(0, num_vertices, dtype=int).to(self.device)

        num_iterations = 0
        while num_iterations != self._max_num_iterations:
            new_incidency_matrix, edge_ids, vertex_ids, edge_demands = self._do_iteration(incidency_matrix, edge_ids, vertex_ids, edge_demands)
            if torch.equal(new_incidency_matrix, incidency_matrix): # TODO: do other values need to be checked?
                break
            num_iterations += 1
            incidency_matrix = new_incidency_matrix

        new_edges = []
        for e in range(incidency_matrix.shape[0]):
            new_edges.append(vertex_ids[incidency_matrix[e, :]].tolist())
        edge_demands = edge_demands.tolist()
        is_reduced = len(edge_ids) < num_edges or any(len(e1) != len(e2) for e1, e2 in zip(edges, new_edges))

        in_hitting_set = []

        return new_edges, edge_demands, in_hitting_set, is_reduced

    def _do_iteration(self, incidency_matrix, edge_ids, vertex_ids, edge_demands):
        for reduction in [self._reduce_edges,
                          self._reduce_vertices,
                          self._optional_reductions]:
            incidency_matrix, edge_ids, vertex_ids, edge_demands = reduction(incidency_matrix, edge_ids, vertex_ids, edge_demands)
        return incidency_matrix, edge_ids, vertex_ids, edge_demands

    @abc.abstractmethod
    def _reduce_edges(self, incidency_matrix, edge_ids, vertex_ids, edge_demands):
        raise NotImplementedError

    @abc.abstractmethod
    def _reduce_vertices(self, incidency_matrix, edge_ids, vertex_ids, edge_demands):
        raise NotImplementedError

    @abc.abstractmethod
    def _optional_reductions(self, incidency_matrix, edge_ids, vertex_ids, edge_demands):
        raise NotImplementedError

    def _order_domination(self, can_dominate):
        size = can_dominate.shape[0]
        return can_dominate & (~torch.triu(can_dominate)).T

    @property
    def is_available(self):
        return IS_TORCH_AVAILABLE


class WeiheGpuReducer(WeiheGpuPythonReducer):
    def __init__(self, **kwargs):
        super(WeiheGpuReducer, self).__init__(**kwargs)

    def _reduce_edges(self, incidency_matrix, edge_ids, vertex_ids, edge_demands):
        float_incidency_matrix = incidency_matrix.float()
        edge_lengths = torch.sum(incidency_matrix, axis=1)
        float_edge_intersections = torch.mm(float_incidency_matrix, float_incidency_matrix.T)
        del float_incidency_matrix
        # can_dominate[i, j] = E_i \subseteq E_j
        can_dominate = _float_tensor_is_zero(float_edge_intersections - edge_lengths.float()).T
        del float_edge_intersections
        del edge_lengths
        is_dominating = self._order_domination(can_dominate)
        del can_dominate
        left_edges = torch.sum(is_dominating.T, axis=1) == 0
        del is_dominating

        new_incidency_matrix = incidency_matrix[left_edges, :]
        new_edge_ids = edge_ids[left_edges]
        new_edge_demands = edge_demands[left_edges]
        return new_incidency_matrix, new_edge_ids, vertex_ids, new_edge_demands

    def _reduce_vertices(self, incidency_matrix, edge_ids, vertex_ids, edge_demands):
        float_incidency_matrix = incidency_matrix.float()
        vertex_lengths = torch.sum(incidency_matrix.T, axis=1)
        float_vertex_intersections = torch.mm(float_incidency_matrix.T, float_incidency_matrix)
        del float_incidency_matrix
        # can_dominate[i, j] = V_i \superseteq V_j
        can_dominate = _float_tensor_is_zero(float_vertex_intersections - vertex_lengths.float())
        del float_vertex_intersections
        del vertex_lengths
        is_dominating = self._order_domination(can_dominate)
        del can_dominate
        left_vertices = torch.sum(is_dominating.T, axis=1) == 0
        del is_dominating

        new_incidency_matrix = incidency_matrix[:, left_vertices]
        new_vertex_ids = vertex_ids[left_vertices]
        return new_incidency_matrix, edge_ids, new_vertex_ids, edge_demands

    def _optional_reductions(self, incidency_matrix, edge_ids, vertex_ids, edge_demands):
        return incidency_matrix, edge_ids, vertex_ids, edge_demands


class FWeiheGpuReducer(WeiheGpuPythonReducer):
    def __init__(self, **kwargs):
        super(FWeiheGpuReducer, self).__init__(**kwargs)

    def _reduce_edges(self, incidency_matrix, edge_ids, vertex_ids, edge_demands):
        float_incidency_matrix = incidency_matrix.float()
        edge_lengths = torch.sum(incidency_matrix, axis=1)
        float_edge_intersections = torch.mm(float_incidency_matrix, float_incidency_matrix.T)
        del float_incidency_matrix
        # float_edge_difference[i, j] = E_i \ E_j
        float_edge_difference = (edge_lengths - float_edge_intersections).T
        del edge_lengths
        del float_edge_intersections
        # demands_difference[i, j] = demands[i] - demands[j]
        demands_difference = torch.unsqueeze(edge_demands, axis=1) - torch.unsqueeze(edge_demands, axis=0)
        can_dominate = _float_tensor_greater_equal(demands_difference, float_edge_difference)
        del float_edge_difference
        del demands_difference
        is_dominating = self._order_domination(can_dominate)
        del can_dominate
        left_edges = torch.sum(is_dominating.T, axis=1) == 0
        del is_dominating

        new_incidency_matrix = incidency_matrix[left_edges, :]
        new_edge_ids = edge_ids[left_edges]
        new_edge_demands = edge_demands[left_edges]
        return new_incidency_matrix, new_edge_ids, vertex_ids, new_edge_demands

    def _reduce_vertices(self, incidency_matrix, edge_ids, vertex_ids, edge_demands):
        float_incidency_matrix = incidency_matrix.float()
        vertex_lengths = torch.sum(incidency_matrix.T, axis=1)
        float_vertex_intersections = torch.mm(float_incidency_matrix.T, float_incidency_matrix)
        del float_incidency_matrix
        can_dominate = _float_tensor_is_zero(float_vertex_intersections - vertex_lengths.float())
        del float_vertex_intersections
        is_dominating = self._order_domination(can_dominate)
        del can_dominate
        num_dominators = torch.sum(is_dominating.T, axis=1)
        del is_dominating
        required_num_dominators_by_edge = incidency_matrix * torch.unsqueeze(edge_demands, axis=1)
        left_vertices = (num_dominators < required_num_dominators_by_edge).any(axis=0)
        del num_dominators
        del required_num_dominators_by_edge

        new_incidency_matrix = incidency_matrix[:, left_vertices]
        new_vertex_ids = vertex_ids[left_vertices]
        return new_incidency_matrix, edge_ids, new_vertex_ids, edge_demands

    def _optional_reductions(self, incidency_matrix, edge_ids, vertex_ids, edge_demands):
        return incidency_matrix, edge_ids, vertex_ids, edge_demands
