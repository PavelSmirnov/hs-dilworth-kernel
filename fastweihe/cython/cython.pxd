from libcpp cimport bool
from libcpp.memory cimport unique_ptr
from libcpp.string cimport string
from libcpp.vector cimport vector


cdef extern from "common/helpers.h" namespace "fastweihe":
    cdef Hypergraph RemoveEdgesFromHypergraph(const Hypergraph& hypergraph,
                                              const vector[int]& removed_edges_in_order) except +

cdef extern from "common/hypergraph.h" namespace "fastweihe":
    cdef cppclass Hypergraph:
        vector[vector[int]] edges
        vector[vector[int]] vertices
        vector[int] edge_demands

    cdef bool IsHittingSetInstanceConsistent(const Hypergraph& instance) except +
    cdef bool CheckIsOptimalSolution(const Hypergraph& instance) except +
    cdef int GetNumUsedVertices(const Hypergraph& graph) except +

cdef extern from "common/log.h" namespace "fastweihe":
    cdef void OutputLog(const string& log_level_name, const string& message_log_level, const string& message);
    cdef int GetLogLevel(const string& log_level_name) except +
    cdef vector[string] GetLogLevelNames() except +

cdef extern from "reducer/reducer.h" namespace "fastweihe":
    cdef cppclass HittingSetReducer:
        Hypergraph Reduce(const Hypergraph& input, const int log_level, vector[int]* in_hiting_set, bool* is_reduced) except +
        bool IsAvailable() except +

    cdef unique_ptr[HittingSetReducer] MakeReducer(const string& name) except +

cdef extern from "reducer/helpers.h" namespace "fastweihe":
    cdef void AddHittingSetData(const vector[int]& src, vector[int]* dest) except +

cdef extern from "lb_estimator/estimator.h" namespace "fastweihe":
    cdef cppclass LowerBoundEstimator:
        int EstimateLowerBound(const Hypergraph& input, const int log_level) except +

    cdef unique_ptr[LowerBoundEstimator] MakeLowerBoundEstimator(const string& name) except +

cdef extern from "lb_estimator/helpers.h" namespace "fastweihe":
    Hypergraph ConstructPushThroughHypergraph(const Hypergraph& hypergraph,
                                              const int pivot_edge,
                                              const vector[int]& removed_edges_in_order) except +
