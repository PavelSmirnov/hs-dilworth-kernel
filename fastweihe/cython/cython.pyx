from libcpp.memory cimport nullptr, unique_ptr
from libcpp.vector cimport vector
from cython.operator import address, dereference

from fastweihe.cython.cython cimport (AddHittingSetData, CheckIsOptimalSolution,
                                      GetLogLevel, GetLogLevelNames,
                                      GetNumUsedVertices, HittingSetReducer, Hypergraph,
                                      IsHittingSetInstanceConsistent, LowerBoundEstimator,
                                      MakeLowerBoundEstimator, MakeReducer, OutputLog)


def _construct_vertices_from_edges(edges):
    if not any(map(len, edges)):
        num_vertices = 0
    else:
        num_vertices = max(max(edge) for edge in edges if edge) + 1

    vertices = [[] for i in range(num_vertices)]
    for edge_id, edge in enumerate(edges):
        for vertex_id in edge:
            vertices[vertex_id].append(edge_id)

    return vertices


cdef class HypergraphData(object):
    cdef Hypergraph _data

    def __cinit__(self):
        pass

    def set_edges(self, edges, edge_demands):
        self._data.edges = edges

        # TODO: can be done in C++
        self._data.vertices = _construct_vertices_from_edges(edges)
        self._data.edge_demands = edge_demands

    @property
    def edges(self):
        return [[vertex_id for vertex_id in edge] for edge in self._data.edges]

    @property
    def vertices(self):
        return [[edge_id for edge_id in vertex] for vertex in self._data.vertices]

    @property
    def edge_demands(self):
        return [demand for demand in self._data.edge_demands]

    @property
    def num_edges(self):
        return self._data.edges.size()

    @property
    def num_vertices(self):
        return self._data.vertices.size()

    @property
    def num_used_vertices(self):
        return GetNumUsedVertices(self._data)

    @property
    def is_consistent(self):
        return IsHittingSetInstanceConsistent(self._data)

    # TODO: &?
    cdef Hypergraph get_data(self):
        return self._data

    cdef set_data(self, Hypergraph data):
        self._data = data


cdef class HittingSetData(object):
    cdef vector[int] _data

    def __cinit__(self, vertices=[]):
        for vertex in vertices:
            self._data.push_back(vertex)

    @property
    def vertices(self):
        return [vertex for vertex in self._data]

    @property
    def size(self):
        return self._data.size()

    cdef vector[int]* ref(self):
        return address(self._data)

    cdef _add(self, HittingSetData other):
        AddHittingSetData(other._data, address(self._data))

    def add(self, other):
        self._add(other)


cdef class Reducer(object):
    cdef unique_ptr[HittingSetReducer] _reducer

    def __cinit__(self, name, **kwargs):
        self._reducer.reset(MakeReducer(name.encode()).release())
        assert self._reducer.get() != nullptr, f"Unable to create '{name}' reducer."

    cdef _reduce(self, HypergraphData input, string log_level_name, HittingSetData in_hitting_set):
        output = HypergraphData()
        cdef bool is_reduced
        cdef int log_level = GetLogLevel(log_level_name)
        output.set_data(dereference(self._reducer.get()).Reduce(input.get_data(), log_level,
                                                                in_hitting_set.ref(), address(is_reduced)))
        return output, is_reduced

    def reduce(self, input, log_level_name, **kwargs):
        in_hitting_set = HittingSetData()
        output, is_reduced = self._reduce(input, log_level_name.encode('utf8'), in_hitting_set)
        return output, in_hitting_set, is_reduced

    @property
    def is_available(self):
        return dereference(self._reducer.get()).IsAvailable()


def output_log(log_level_name, message_log_level, message):
    OutputLog(log_level_name.encode('utf8'), message_log_level.encode('utf8'), message.encode('utf8'))


def get_log_level_names():
    return [name.decode('utf8') for name in GetLogLevelNames()]


cdef class LBEstimator(object):
    cdef unique_ptr[LowerBoundEstimator] _estimator

    def __cinit__(self, name, **kwargs):
        self._estimator.reset(MakeLowerBoundEstimator(name.encode()).release())
        assert self._estimator.get() != nullptr, f"Unable to create '{name}' lower bound estimator."

    cdef _estimate(self, HypergraphData input, string log_level_name):
        cdef int log_level = GetLogLevel(log_level_name)
        return dereference(self._estimator.get()).EstimateLowerBound(input.get_data(), log_level)

    def estimate(self, input, log_level_name, **kwargs):
        return self._estimate(input, log_level_name.encode('utf8'))


cdef _construct_push_through_hypergraph(HypergraphData hypergraph, pivot_edge, const vector[int]& removed_edges_in_order):
    result = HypergraphData()
    result.set_data(ConstructPushThroughHypergraph(hypergraph.get_data(), pivot_edge, removed_edges_in_order))
    return result


def construct_push_through_hypergraph_data(hypergraph, pivot_edge, removed_edges_in_order):
    return _construct_push_through_hypergraph(hypergraph.data, pivot_edge, removed_edges_in_order)


cdef _remove_edges_from_hypergraph(HypergraphData hypergraph, const vector[int]& removed_edges_in_order):
    result = HypergraphData()
    result.set_data(RemoveEdgesFromHypergraph(hypergraph.get_data(), removed_edges_in_order))
    return result


def remove_edges_from_hypergraph(hypergraph, removed_edges_in_order):
    return _remove_edges_from_hypergraph(hypergraph.data, removed_edges_in_order)


def _check_is_optimal_solution(HypergraphData hypergraph):
    return CheckIsOptimalSolution(hypergraph.get_data())


def check_is_optimal_solution(hypergraph):
    return _check_is_optimal_solution(hypergraph.data)
