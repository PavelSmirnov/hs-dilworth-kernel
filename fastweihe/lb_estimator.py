import abc

from fastweihe.cpp_wrappers import CppLBEstimator
from fastweihe.hypergraph import Hypergraph
from fastweihe.python_lb_estimators import ExactAnswerEstimator, LinearProblemLowerBoundEstimator


class BaseLowerBoundEstimator(abc.ABC):
    def __init__(self, **kwargs):
        pass

    @abc.abstractmethod
    def __call__(self, input, **kwargs):
        """
        Gives a lower bound on solution size for a Hitting Set problem instance.
        Returns an integer.

        Parameters:
        ___________
        hypergraph: fastweihe.Hypergraph
            An input hypergraph.
        """
        raise NotImplementedError


class LowerBoundEstimator(BaseLowerBoundEstimator):
    _CPP_ESTIMATORS = ['trivial', 'max-demand']
    _PYTHON_ESTIMATORS = {'lp': LinearProblemLowerBoundEstimator, 'exact': ExactAnswerEstimator}

    def __init__(self, algorithm, have_cuda=False, **kwargs):
        """
        A class for Hitting Set lower bound estimators.

        Parameters:
        ___________
        algorithm: str
            The lower bound estimation algorithm.
            (See LowerBoundEstimator.get_available_algorithms.)

        have_cuda: bool (default: False)
            If True, a GPU may be used for some estimators.
        """
        super(LowerBoundEstimator, self).__init__(**kwargs)
        self.name = algorithm
        self._estimator = self._choose_estimator(algorithm, have_cuda=have_cuda, **kwargs)

    def _choose_estimator(self, algorithm, have_cuda, **kwargs):
        if algorithm in self._CPP_ESTIMATORS:
            return CppLBEstimator(algorithm, **kwargs)
        if algorithm in self._PYTHON_ESTIMATORS:
            estimator_cls = self._PYTHON_ESTIMATORS[algorithm]
            return estimator_cls(have_cuda=have_cuda, **kwargs)
        assert False, f"Unknown estimator '{algorithm}'."

    def __call__(self, input, log_level='error', **kwargs):
        assert isinstance(input, Hypergraph), "LowerBoundEstimator input must be a hypergraph."
        return self._estimator(input, log_level=log_level, **kwargs)

    @classmethod
    def get_available_algorithms(cls):
        """
        Returns a list of available algorithms.
        Any of them can be passed to the LowerBoundEstimator constructor.
        """
        return list(set(cls._CPP_ESTIMATORS).union(cls._PYTHON_ESTIMATORS.keys()))

    def __repr__(self):
        return f"{self.name}"


class MaxLowerBoundEstimator(BaseLowerBoundEstimator):
    def __init__(self, estimators, **kwargs):
        """
        A Hitting Set size lower bound estimator giving the best lower bound from a set of estimators.

        Parameters:
        ___________
        estimators: list of estimators or strs
            Each str converts to the corresponding estimator.
            (See LowerBoundEstimator.get_available_algorithms.)
        """
        super(MaxLowerBoundEstimator, self).__init__(**kwargs)

        self._estimators = []
        for estimator in estimators:
            self._estimators.append(compose_lower_bound_estimator(estimator))
        assert self._estimators, 'No lower bound estimators given!'

    def __call__(self, input, **kwargs):
        return max(estimator(input, **kwargs) for estimator in self._estimators)

    def __repr__(self):
        names = ', '.join(map(repr, self._estimators))
        return f"max({names})"


def compose_lower_bound_estimator(composition, **kwargs):
    if isinstance(composition, BaseLowerBoundEstimator):
        return composition
    if isinstance(composition, str):
        return LowerBoundEstimator(algorithm=composition, **kwargs)
    if isinstance(composition, tuple):
        return MaxLowerBoundEstimator([compose_lower_bound_estimator(estimator, **kwargs) for estimator in composition], **kwargs)
    assert False, f"Can't parse lower bound estimator: {composition}"
