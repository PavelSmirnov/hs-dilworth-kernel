# fastweihe

This is a Hitting Set data reduction algorithms Python package.

## Prerequsites

1. `Python 3.7+`

## Installation

You can install the package using `pip` package manager:
```
pip install .
```

If you want to have a `solve-gurobi` reducer, you need to provide a Gurobi installation directory:
```
GUROBI_PATH=/path/to/gurobi pip install .
```

## Development

You may want to install the package like this:
```
pip install -e .
```
This allows not to rebuild the package to see the changes in Python code.

**Important.** Before commiting any changes ensure that all tests are passed by running:
```
py.test tests
```

### C++

To implement a C++ algorithm which is not implemented yet do the following steps:

1. Pick the algorithm name from `_NOT_IMPLEMENTED_REDUCERS` in `fastweihe/reducer.py` and remove it from the list.

2. Implement the algorithm in C++ in `src` directory.
The new algorithm class must be a subclass of `HittingSetReducer`.
(See an example algorithm `IdentityReducer`.)

3. Add the new algorithm in `MakeReducer` function implementation in `src/reducer.cpp`.
(See an example algorithm `IdentityReducer`.)

4. Rebuild the library and test.

### Python

To implement a Python algorithm do the following steps:

1. Create a `fastweihe.python_reducers.PythonReducer` subclass implementing `_reduce` method.

2. Change `fastweihe.reducer.HittingSetReducer` attribute `_NOT_IMPLEMENTED_REDUCERS` accordingly.

3. Test.
