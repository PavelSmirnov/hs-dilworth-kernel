#include "f_weihe_fast.h"
#include "helpers.h"
#include "common/helpers.h"
#include "common/log.h"
#include <cassert>
#include <numeric>
#include <queue>
#include <vector>

namespace fastweihe {
    namespace {
        constexpr int NO_ITERATION = -1;

        void StageEdgesForRemoval(const int log_level,
                                  const bool make_simple_edge_reduction,
                                  const IntersectionMatrix& edge_intersection_sizes,
                                  const std::vector<int>& dominator_domain,
                                  const std::vector<int>& edge_sizes,
                                  const std::vector<int>& edge_demands,
                                  const std::vector<int>& alive_edges,
                                  std::vector<bool>* is_edge_staged_for_removal,
                                  std::queue<int>* edges_to_remove) {
            assert(is_edge_staged_for_removal);
            assert(edges_to_remove);

            FASTWEIHE_INFO(log_level, "Iterating over " << alive_edges.size() << " candidate edges for removal\n");
            FASTWEIHE_INFO(log_level, "Number of potential_dominators is " << dominator_domain.size() << ".\n");
            FASTWEIHE_INFO(log_level, "At most " << alive_edges.size() * dominator_domain.size() << " domination relations are to be checked.\n");
            for (const int candidate_edge : alive_edges) {
                FASTWEIHE_DEBUG(log_level, "Checking edge " << candidate_edge << ".");
                const int candidate_size = edge_sizes[candidate_edge];
                for (const auto potential_dominant_edge : dominator_domain) {
                    if ((*is_edge_staged_for_removal)[potential_dominant_edge] ||
                        candidate_edge == potential_dominant_edge) {
                        continue;
                    }
                    const int potential_dominant_size = edge_sizes[potential_dominant_edge];
                    const int intersection_size = edge_intersection_sizes(candidate_edge, potential_dominant_edge);
                    const int dominant_without_candidate_size = potential_dominant_size - intersection_size;
                    const int candidate_without_dominant_size = candidate_size - intersection_size;

                    if (make_simple_edge_reduction) {
                        if (dominant_without_candidate_size > 0 ||
                            edge_demands[candidate_edge] > edge_demands[potential_dominant_edge]) {
                            continue;
                        }
                        if (candidate_without_dominant_size == 0 &&
                            edge_demands[candidate_edge] == edge_demands[potential_dominant_edge] &&
                            candidate_edge < potential_dominant_edge) {
                            continue;
                        }
                    } else {
                        if (edge_demands[candidate_edge] == edge_demands[potential_dominant_edge] &&
                            candidate_without_dominant_size == 0 &&
                            dominant_without_candidate_size == 0 &&
                            candidate_edge < potential_dominant_edge) {
                            continue;
                        }
                        if (edge_demands[candidate_edge] > edge_demands[potential_dominant_edge] - dominant_without_candidate_size) {
                            continue;
                        }
                    }
                    FASTWEIHE_DEBUG(log_level, " Dominated!");
                    (*is_edge_staged_for_removal)[candidate_edge] = true;
                    edges_to_remove->push(candidate_edge);
                    break;
                }
                FASTWEIHE_DEBUG(log_level, "\n");
            }
        }

        void StageVerticesForRemoval(const int log_level,
                                     const IntersectionMatrix& vertex_intersection_sizes,
                                     const std::vector<std::vector<int>>& vertices,
                                     const std::vector<int>& candidate_domain,
                                     const std::vector<int>& vertex_sizes,
                                     const std::vector<int>& edge_demands,
                                     const std::vector<bool>& is_edge_staged_for_removal,
                                     const std::vector<int>& alive_vertices,
                                     std::vector<bool>* is_vertex_staged_for_removal,
                                     std::queue<int>* vertices_to_remove) {
            assert(is_vertex_staged_for_removal);
            assert(vertices_to_remove);

            FASTWEIHE_INFO(log_level, "Iterating over " << candidate_domain.size() << " candidate vertices for removal\n");
            FASTWEIHE_INFO(log_level, "Number of potential_dominators is " << alive_vertices.size() << ".\n");
            FASTWEIHE_INFO(log_level, "At most " << candidate_domain.size() * alive_vertices.size() << " domination relations are to be checked.\n");
            for (const auto candidate_vertex : candidate_domain) {
                FASTWEIHE_DEBUG(log_level, "Checking vertex " << candidate_vertex << ".");
                int required_num_dominators = 0;
                for (const auto neighbor_edge : vertices[candidate_vertex]) {
                    const int edge_demand = edge_demands[neighbor_edge];
                    if (!is_edge_staged_for_removal[neighbor_edge] && edge_demand > required_num_dominators) {
                        required_num_dominators = edge_demand;
                    }
                }
                FASTWEIHE_DEBUG(log_level, " Searching for " << required_num_dominators << " dominators.");

                const int candidate_size = vertex_sizes[candidate_vertex];
                int num_dominators = 0;
                for (int potential_dominant_vertex : alive_vertices) {
                    if (num_dominators == required_num_dominators) {
                        break;
                    }

                    if ((*is_vertex_staged_for_removal)[potential_dominant_vertex] ||
                        candidate_vertex == potential_dominant_vertex) {
                        continue;
                    }
                    const int potential_dominant_size = vertex_sizes[potential_dominant_vertex];
                    const int intersection_size = vertex_intersection_sizes(candidate_vertex, potential_dominant_vertex);
                    if (intersection_size < candidate_size) {
                        continue;
                    }
                    if (candidate_size == potential_dominant_size && candidate_vertex < potential_dominant_vertex) {
                        continue;
                    }
                    ++num_dominators;
                    FASTWEIHE_DEBUG(log_level, " Found dominator.");
                }
                if (num_dominators == required_num_dominators) {
                    FASTWEIHE_DEBUG(log_level, " Dominated!");
                    (*is_vertex_staged_for_removal)[candidate_vertex] = true;
                    vertices_to_remove->push(candidate_vertex);
                }
                FASTWEIHE_DEBUG(log_level, "\n");
            }
        }

        void ReduceEdges(const int log_level,
                         const std::vector<std::vector<int>>& edges,
                         const std::vector<std::vector<int>>& vertices,
                         const std::vector<int>& edge_demands,
                         const std::vector<bool>& is_edge_staged_for_removal,
                         const bool first_loop,
                         const int weihe_iteration,
                         std::vector<int>* last_vertex_affection_iteration,
                         std::vector<int>* alive_vertices,
                         std::vector<int>* vertex_sizes,
                         std::vector<bool>* is_vertex_staged_for_removal,
                         IntersectionMatrix* vertex_intersection_sizes,
                         std::queue<int>* edges_to_remove,
                         std::queue<int>* vertices_to_remove) {
            assert(alive_vertices);
            assert(vertex_sizes);
            assert(is_vertex_staged_for_removal);
            assert(vertex_intersection_sizes);
            assert(edges_to_remove);
            assert(vertices_to_remove);

            FASTWEIHE_INFO(log_level, "Removing " << edges_to_remove->size() << " edges from the removal queue.\n");
            std::vector<int> candidates;
            while (!edges_to_remove->empty()) {
                const auto removed_edge = edges_to_remove->front();
                edges_to_remove->pop();
                const auto& edge_vertices = edges[removed_edge];
                RemoveSet(edge_vertices, vertex_sizes, vertex_intersection_sizes);
                candidates.reserve(candidates.size() + edge_vertices.size());
                for (const int vertex : edge_vertices) {
                    if ((*is_vertex_staged_for_removal)[vertex]) {
                        continue;
                    }
                    auto& affection_iteration = (*last_vertex_affection_iteration)[vertex];
                    if (affection_iteration == weihe_iteration) {
                        continue;
                    }
                    affection_iteration = weihe_iteration;
                    candidates.push_back(vertex);
                }
            }

            FASTWEIHE_INFO(log_level, "Staging vertices for removal.\n");
            StageVerticesForRemoval(log_level,
                                    *vertex_intersection_sizes,
                                    vertices,
                                    first_loop ? *alive_vertices : candidates,
                                    *vertex_sizes,
                                    edge_demands,
                                    is_edge_staged_for_removal,
                                    *alive_vertices,
                                    is_vertex_staged_for_removal,
                                    vertices_to_remove);

            FASTWEIHE_INFO(log_level, "Updating alive vertices.");
            UpdateAliveElements(*is_vertex_staged_for_removal, alive_vertices);
            FASTWEIHE_INFO(log_level, " " << alive_vertices->size() << " vertices are alive.\n");
        }

        void ReduceVertices(const int log_level,
                            const bool make_simple_edge_reduction,
                            const std::vector<std::vector<int>>& vertices,
                            const std::vector<int>& edge_demands,
                            const bool first_loop,
                            const int weihe_iteration,
                            std::vector<int>* last_edge_affection_iteration,
                            std::vector<int>* alive_edges,
                            std::vector<int>* edge_sizes,
                            std::vector<bool>* is_edge_staged_for_removal,
                            IntersectionMatrix* edge_intersection_sizes,
                            std::queue<int>* vertices_to_remove,
                            std::queue<int>* edges_to_remove) {
            assert(alive_edges);
            assert(edge_sizes);
            assert(is_edge_staged_for_removal);
            assert(edge_intersection_sizes);
            assert(vertices_to_remove);
            assert(edges_to_remove);

            FASTWEIHE_INFO(log_level, "Removing " << vertices_to_remove->size() << " vertices from the removal queue.\n");
            std::vector<int> potential_dominators;
            while (!vertices_to_remove->empty()) {
                const auto removed_vertex = vertices_to_remove->front();
                vertices_to_remove->pop();
                const auto& vertex_edges = vertices[removed_vertex];
                RemoveSet(vertex_edges, edge_sizes, edge_intersection_sizes);
                potential_dominators.reserve(potential_dominators.size() + vertex_edges.size());
                for (const int edge : vertex_edges) {
                    if ((*is_edge_staged_for_removal)[edge]) {
                        continue;
                    }
                    auto& affection_iteration = (*last_edge_affection_iteration)[edge];
                    if (affection_iteration == weihe_iteration) {
                        continue;
                    }
                    affection_iteration = weihe_iteration;
                    potential_dominators.push_back(edge);
                }
            }

            FASTWEIHE_INFO(log_level, "Staging edges for removal.\n");
            StageEdgesForRemoval(log_level,
                                 make_simple_edge_reduction,
                                 *edge_intersection_sizes,
                                 first_loop ? *alive_edges : potential_dominators,
                                 *edge_sizes,
                                 edge_demands,
                                 *alive_edges,
                                 is_edge_staged_for_removal,
                                 edges_to_remove);

            FASTWEIHE_INFO(log_level, "Updating alive edges.");
            UpdateAliveElements(*is_edge_staged_for_removal, alive_edges);
            FASTWEIHE_INFO(log_level, " " << alive_edges->size() << " edges are alive.\n");
        }
    }

    Hypergraph FWeiheFastReducer::Reduce(const Hypergraph& input,
                                         const int log_level,
                                         std::vector<int>* in_hitting_set,
                                         bool* is_reduced) const {
        assert(is_reduced);

        FASTWEIHE_INFO(log_level, "\n"
                                  "---------------------------\n"
                                  "Starting f-Weihe reduction.\n");
        const int num_edges = input.edges.size();
        const int num_vertices = input.vertices.size();

        std::vector<int> edge_sizes = CalcSetSizes(input.edges);
        std::vector<int> vertex_sizes = CalcSetSizes(input.vertices);

        FASTWEIHE_INFO(log_level, "Calculating edge intersection sizes.");
        auto edge_intersection_sizes = InitializeSetIntersectionSizes(input.edges, input.vertices);
        FASTWEIHE_INFO(log_level, " Done!\n");
        FASTWEIHE_INFO(log_level, "Calculating vertex intersection sizes.");
        auto vertex_intersection_sizes = InitializeSetIntersectionSizes(input.vertices, input.edges);
        FASTWEIHE_INFO(log_level, " Done!\n");

        std::vector<int> alive_edges(num_edges);
        std::iota(alive_edges.begin(), alive_edges.end(), 0);
        FASTWEIHE_INFO(log_level, alive_edges.size() << " edges are alive.\n");
        std::vector<int> alive_vertices;
        alive_vertices.reserve(num_vertices);
        for (int vertex = 0; vertex < num_vertices; ++vertex) {
            if (!input.vertices[vertex].empty()) {
                alive_vertices.push_back(vertex);
            }
        }
        FASTWEIHE_INFO(log_level, alive_vertices.size() << " vertices are alive.\n");

        std::vector<int> last_edge_affection_iteration(num_edges, NO_ITERATION);
        std::vector<int> last_vertex_affection_iteration(num_vertices, NO_ITERATION);

        std::vector<bool> is_edge_staged_for_removal(num_edges, false);
        std::vector<bool> is_vertex_staged_for_removal(num_vertices, false);
        std::queue<int> edges_to_remove;
        std::queue<int> vertices_to_remove;

        bool first_loop = true;
        int weihe_iteration = 0;
        FASTWEIHE_INFO(log_level, "Starting main loop.\n");
        while (true) {
            ReduceVertices(log_level,
                           MakeSimpleEdgeReduction(),
                           input.vertices,
                           input.edge_demands,
                           first_loop,
                           weihe_iteration,
                           &last_edge_affection_iteration,
                           &alive_edges,
                           &edge_sizes,
                           &is_edge_staged_for_removal,
                           &edge_intersection_sizes,
                           &vertices_to_remove,
                           &edges_to_remove);
            if (edges_to_remove.empty() && !first_loop) {
                break;
            }

            ++weihe_iteration;

            ReduceEdges(log_level,
                        input.edges,
                        input.vertices,
                        input.edge_demands,
                        is_edge_staged_for_removal,
                        first_loop,
                        weihe_iteration,
                        &last_vertex_affection_iteration,
                        &alive_vertices,
                        &vertex_sizes,
                        &is_vertex_staged_for_removal,
                        &vertex_intersection_sizes,
                        &edges_to_remove,
                        &vertices_to_remove);
            if (vertices_to_remove.empty()) {
                break;
            }
            first_loop = false;
        }

        FASTWEIHE_INFO(log_level, "Checking whether instance is reduced at all\n");
        *is_reduced = IsReduced(is_vertex_staged_for_removal, is_edge_staged_for_removal);

        FASTWEIHE_INFO(log_level, "Constructing output\n");
        return ConstructOutput(input.edges,
                               num_vertices,
                               is_edge_staged_for_removal,
                               is_vertex_staged_for_removal,
                               input.edge_demands);
    }

    bool FWeiheFastReducer::MakeSimpleEdgeReduction() const {
        return false;
    }
}
