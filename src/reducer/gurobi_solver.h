#pragma once
#include "reducer.h"

namespace fastweihe {
    class GurobiSolverReducer : public HittingSetReducer {
    public:
        Hypergraph Reduce(const Hypergraph& input,
                          const int log_level,
                          std::vector<int>* in_hitting_set,
                          bool* is_reduced) const override;
        bool IsAvailable() const override;
    };
}
