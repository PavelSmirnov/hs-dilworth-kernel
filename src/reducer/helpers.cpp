#include "helpers.h"
#include <algorithm>
#include <cassert>
#include <iostream>
#include <set>

namespace fastweihe {
    IntersectionMatrix::IntersectionMatrix(const int size) {
        _data.resize(size);
        for (int i = 0; i < size; ++i) {
            _data[i].resize(size - i);
        }
    }

    std::vector<int> CalcSetSizes(const std::vector<std::vector<int>>& sets) {
        const int num_sets = sets.size();
        std::vector<int> sizes;
        sizes.reserve(num_sets);
        for (const auto& one_set : sets) {
            sizes.push_back(one_set.size());
        }
        return sizes;
    }

    IntersectionMatrix InitializeSetIntersectionSizes(const std::vector<std::vector<int>>& sets,
                                                   const std::vector<std::vector<int>>& elements) {
        const int num_sets = sets.size();
        IntersectionMatrix intersection_sizes(num_sets);
        for (int first_set = 0; first_set < num_sets; ++first_set) {
            for (const int element : sets[first_set]) {
                for (const int second_set : elements[element]) {
                    if (second_set <= first_set) {
                        continue;
                    }
                    intersection_sizes.Inc(first_set, second_set);
                }
            }
        }
        return intersection_sizes;
    }

    void RemoveSet(const std::vector<int>& element_set,
                   std::vector<int>* element_sizes,
                   IntersectionMatrix* element_intersection_sizes) {
        assert(element_sizes);
        assert(element_intersection_sizes);

        for (const auto element : element_set) {
            --(*element_sizes)[element];
        }

        const int num_elements = element_set.size();
        for (int element_pos = 0; element_pos < num_elements; ++element_pos) {
            for (int other_element_pos = 0; other_element_pos < element_pos; ++other_element_pos) {
                element_intersection_sizes->Dec(element_set[element_pos], element_set[other_element_pos]);
            }
        }
    }

    Hypergraph ConstructOutput(const std::vector<std::vector<int>>& edges,
                               const int num_vertices,
                               const std::vector<bool>& is_edge_removed,
                               const std::vector<bool>& is_vertex_removed,
                               const std::vector<int>& edge_demands) {
        Hypergraph graph;
        const int num_edges = edges.size();
        graph.vertices.resize(num_vertices);
        for (int edge = 0; edge < num_edges; ++edge) {
            if (is_edge_removed[edge]) {
                continue;
            }
            const int renamed_edge = graph.edges.size();
            auto& edge_vertices = graph.edges.emplace_back();
            for (const auto vertex : edges[edge]) {
                if (!is_vertex_removed[vertex]) {
                    edge_vertices.push_back(vertex);
                    graph.vertices[vertex].push_back(renamed_edge);
                }
            }
            graph.edge_demands.push_back(edge_demands[edge]);
        }
        while (graph.vertices.size() && graph.vertices.back().empty()) {
            graph.vertices.pop_back();
        }
        return graph;
    }

    bool IsReduced(const std::vector<bool>& is_vertex_removed, const std::vector<bool>& is_edge_removed) {
        return std::find(is_vertex_removed.begin(), is_vertex_removed.end(), true) != is_vertex_removed.end() ||
               std::find(is_edge_removed.begin(), is_edge_removed.end(), true) != is_edge_removed.end();
    }

    void AddHittingSetData(const std::vector<int>& src, std::vector<int>* dest) {
        assert(dest);

        dest->reserve(dest->size() + src.size());
        for (const int vertex : src) {
            dest->push_back(vertex);
        }
    }

    bool AreHypergraphsEqual(const Hypergraph& first, const Hypergraph& second) {
        const int num_edges = first.edge_demands.size();
        if (static_cast<int>(second.edge_demands.size()) != num_edges) {
            return false;
        }
        for (int edge = 0; edge < num_edges; ++edge) {
            if (first.edge_demands[edge] != second.edge_demands[edge]) {
                return false;
            }
            if (std::set(first.edges[edge].begin(), first.edges[edge].end()) !=
                std::set(second.edges[edge].begin(), second.edges[edge].end())) {
                return false;
            }
        }
        return true;
    }

    void UpdateAliveElements(const std::vector<bool>& is_element_alive, std::vector<int>* alive_elements) {
        assert(alive_elements);

        std::vector<int> new_alive_elements;
        new_alive_elements.reserve(alive_elements->size());
        for (const int element : *alive_elements) {
            if (!is_element_alive[element]) {
                new_alive_elements.push_back(element);
            }
        }
        std::swap(*alive_elements, new_alive_elements);
    }
}
