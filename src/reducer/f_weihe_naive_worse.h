#pragma once
#include "f_weihe_naive.h"

namespace fastweihe {
    class FWeiheNaiveWorseReducer : public FWeiheNaiveReducer {
    protected:
        bool MakeSimpleEdgeReduction() const override;
    };
}
