#include "helpers.h"
#include "weihe_naive.h"
#include "common/helpers.h"
#include <algorithm>
#include <cassert>
#include <vector>


/*Algorithm:
1. The incoming Hypergraph structure is being modified:
1.1 edges and verticals become vector<pair<int,vector<int>>>, where the number of the corresponding edge/vertex is entered in int.
2. The modified Hypergraph is included in the reduction cycle:
2.1 Reducing edges by the rule "if an edge Ei is included in an edge Ej, then delete Ej":
2.1.1 Making nested loops. The outer (i) runs from the beginning to the end of the edge array, and the inner (o) runs from i+1 to the end.
2.1.2 Compare the edges of the cycles.
2.1.3 If the reduction condition for an edge was met during comparison, then we remove it from the edge vector, erase all vertexes that belong to it.
2.2 Reducing vertexes by the rule "if vertex Vi is contained in the same edges as Vj, then delete Vi".
2.2.1-3 Similar to the algorithm for edges, but if the reduction has occurred we raise the change flag.
2.3 If the change flag was raised, we continue the reduction cycle, otherwise we end it.
3. Turning a modified Hypergraph into a regular One (with the addition of imaginary vertexes instead of deleted 
ones and renumber edges in vertexes according to the order of edges after reduction).
4. Passing the final Hypergraph as a result.
*/

namespace fastweihe {
    namespace {
        struct ModifedHypergraph {
            std::vector<std::pair<int, std::vector<int>>> edges;
            std::vector<std::pair<int, std::vector<int>>> vertices;
            std::vector<std::pair<int, int>> edge_demands;

            ModifedHypergraph();

            ModifedHypergraph(const Hypergraph& input) {
                int num_edge = 0;
                int num_vertex = 0;
                int num_demand = 0;
                for (const auto& it : input.edges){
                    edges.push_back(std::make_pair(num_edge++, it));
                }
                for (const auto& it : input.vertices){
                    vertices.push_back(std::make_pair(num_vertex++, it));
                }
                for (const auto& it : input.edge_demands) {
                    edge_demands.push_back(std::make_pair(num_demand++, it));
                }
            }

            auto GetEdgeIter(const int edge_number) {
                for (auto edge_iter = edges.begin(); edge_iter != edges.end(); edge_iter++) {
                    if (edge_iter->first == edge_number) {
                        return edge_iter;
                    }
                }
                return edges.end();
            }

            auto GetVertexIter(const int vertex_number) {
                for (auto vertex_iter = vertices.begin(); vertex_iter != vertices.end(); vertex_iter++) {
                    if (vertex_iter->first == vertex_number) {
                        return vertex_iter;
                    }
                }
                return vertices.end();
            }

            auto GetDemandIter(const int demand_number) {
                for (auto demand_iter = edge_demands.begin(); demand_iter != edge_demands.end(); demand_iter++) {
                    if (demand_iter->first == demand_number) {
                        return demand_iter;
                    }
                }
                return edge_demands.end();
            }

            Hypergraph Unmodify() { //TODO: rebuild the algorithm so that the object does not change. Just because
                Hypergraph remod;
                auto vertexs_iter = vertices.begin();
                if (edges.size() != 0) {
                    for (auto edge = edges.begin(); edge != edges.end(); edge++) {
                        remod.edges.push_back(edge->second);
                        for (const auto& edges_vertixs_number : edge->second) {
                            for (vertexs_iter = vertices.begin(); vertexs_iter != vertices.end(); vertexs_iter++) {
                                if (vertexs_iter->first == edges_vertixs_number) {
                                    std::replace(vertexs_iter->second.begin(), vertexs_iter->second.end(), edge->first, int(edge - edges.begin()));
                                }
                            }
                        }
                    }
                }
                else {
                    remod.edges = {};
                }

                vertexs_iter = vertices.begin();
                if (vertices.size() != 0) {
                    for (int num = 0; num <= (vertices.end() - 1)->first; num++) {
                        if (num != vertexs_iter->first) {
                            remod.vertices.push_back(std::vector<int>{});
                        }
                        else {
                            remod.vertices.push_back(vertexs_iter->second);
                            vertexs_iter++;
                        }
                    }
                }
                else {
                    remod.vertices = {};
                }

                if (edge_demands.size() != 0) {
                    for (const auto& demand : edge_demands) {
                        remod.edge_demands.push_back(demand.second);
                    }
                }
                return remod;
            }
        };
    }

    namespace {
        bool IsAInB(const std::vector<int> &a_vector, const std::vector<int> &b_vector);
        bool DoesAHaveB(const std::vector<int>& a_vector, const std::vector<int>& b_vector);
        /*Function for performing steps 2.1.1-3, 2.2.1-3 of the algorithm.
        reduction_target passes 'E' - Edges or 'V' - Vertices,
        hypergraph passes a link to a hypergraph that is being reduced
        Returns the vector of elements of the deleted set*/
        bool CompareAndDelete(const char reduction_target, ModifedHypergraph* hypergraph);
        /*Function for clearing vertices/edges from deleted edges/vertices.
        target passes the number of the deleted vector element,
        numbers_for_cleaning passes numbers of elements of the vector_for_cleaning vector to delete target from,
        vector_for_cleaning passes a vector from the elements of which you need to clean target.*/
        void CleanOtherVector(const int target, const std::vector<int>& numbers_for_cleaning, std::vector<std::pair<int, std::vector<int>>>& vector_for_cleaning);
    }

    Hypergraph WeiheNaiveReducer::Reduce(const Hypergraph& input,
                                         const int log_level,
                                         std::vector<int>* in_hitting_set,
                                         bool* is_reduced) const {
        assert(is_reduced);

        ModifedHypergraph modifed_hypergraph(SortHypergraphEdgesAndVertices(input));
        bool there_is_change = true;
        while (there_is_change) {
            there_is_change = false;
            /*Reduction of edges*/
            CompareAndDelete('E', &modifed_hypergraph);
            /*The reduction of vertices*/
            there_is_change = CompareAndDelete('V', &modifed_hypergraph);
        }

        const auto output = modifed_hypergraph.Unmodify();
        *is_reduced = !AreHypergraphsEqual(input, output);
        return output;
    }

    namespace {

        bool IsAInB(const std::vector<int> &a_vector, const std::vector<int> &b_vector) {
            bool result = true;
            bool there_is_equal_number;
            if (a_vector.size() <= b_vector.size()) {
                for (const int& i : a_vector) {
                    there_is_equal_number = false;
                    for (const int& j : b_vector) {
                        if (j > i) {
                            break;
                        }
                        else if (i == j) {
                            there_is_equal_number = true;
                        }
                    }
                    if (!there_is_equal_number) {
                        result = false;
                        break;
                    }
                }
            }
            else {
                result = false;
            }
            return result;
        }

        bool DoesAHaveB(const std::vector<int>& a_vector, const std::vector<int>& b_vector) {
            return IsAInB(b_vector, a_vector);
        }

        bool CompareAndDelete(const char reduction_target, ModifedHypergraph* hypergraph) {
            std::vector<std::pair<int, std::vector<int>>> *vector_for_deleting = nullptr;
            std::vector<std::pair<int, std::vector<int>>> *vector_for_cleaning = nullptr;
            bool (*CompareF)(const std::vector<int>&, const std::vector<int>&) = nullptr;
            if (reduction_target == 'E') {
                vector_for_deleting = &hypergraph->edges;
                vector_for_cleaning = &hypergraph->vertices;
                CompareF = IsAInB;
            }
            else if (reduction_target == 'V') {
                vector_for_deleting = &hypergraph->vertices;
                vector_for_cleaning = &hypergraph->edges;
                CompareF = DoesAHaveB;
            }
            bool there_is_change = false;
            for (auto iti = vector_for_deleting->begin(); iti != vector_for_deleting->end();) {
                for (auto itj = iti + 1;;) {
                    if (itj == vector_for_deleting->end()) {
                        iti++;
                        break;
                    }
                    if (CompareF(iti->second, itj->second)) {
                        if (reduction_target == 'E') {
                            hypergraph->edge_demands.erase(hypergraph->GetDemandIter(itj->first));
                        }
                        CleanOtherVector(itj->first, itj->second, *vector_for_cleaning);
                        itj = vector_for_deleting->erase(itj);
                        there_is_change = true;
                    }
                    else if (CompareF(itj->second, iti->second)) {
                        if (reduction_target == 'E') {
                            hypergraph->edge_demands.erase(hypergraph->GetDemandIter(iti->first));
                        }
                        CleanOtherVector(iti->first, iti->second, *vector_for_cleaning);
                        iti = vector_for_deleting->erase(iti);
                        there_is_change = true;
                        break;
                    }
                    else {
                        itj++;
                    }
                }
            }
            return there_is_change;
        }

        void CleanOtherVector(const int target, const std::vector<int>& numbers_for_cleaning, std::vector<std::pair<int, std::vector<int>>>& vector_for_cleaning) {
            for (const int& iti : numbers_for_cleaning) {
                for (auto itj = vector_for_cleaning.begin(); itj != vector_for_cleaning.end();itj++) {
                    if (iti == itj->first) {
                        itj->second.erase(std::find(itj->second.begin(), itj->second.end(), target));
                        break;
                    }
                }
            }
        }
    }
}
