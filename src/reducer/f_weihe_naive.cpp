#include "f_weihe_naive.h"
#include "helpers.h"
#include "common/helpers.h"
#include <algorithm>
#include <cassert>
#include <iostream>
#include <numeric>

namespace fastweihe {
    namespace {
        void MovePosition(const std::vector<int>& elements,
                          const int element_upper_bound,
                          const std::vector<bool>& is_element_removed,
                          int* num_skipped_elements,
                          int* position) {
            assert(num_skipped_elements);
            assert(position);

            *num_skipped_elements = 0;
            while (*position < static_cast<int>(elements.size())) {
                const bool element_exists = !is_element_removed[elements[*position]];
                if (element_exists && elements[*position] >= element_upper_bound) {
                    break;
                }
                *num_skipped_elements += element_exists;
                ++(*position);
            }
        }

        void CalcSetDifferences(const std::vector<int>& first_set_elements,
                                const std::vector<int>& second_set_elements,
                                const std::vector<bool>& is_element_removed,
                                int* first_without_second_size,
                                int* second_without_first_size) {
            assert(first_without_second_size);
            assert(second_without_first_size);

            *first_without_second_size = 0;
            *second_without_first_size = 0;
            int second_set_element_pos = 0;
            const int first_set_size = first_set_elements.size();
            for (int first_set_element_pos = 0; first_set_element_pos <= first_set_size; ++first_set_element_pos) {
                const int num_elements = is_element_removed.size();
                int first_set_element;
                if (first_set_element_pos == first_set_size) {
                    first_set_element = num_elements;
                } else {
                    first_set_element = first_set_elements[first_set_element_pos];
                    if (is_element_removed[first_set_element]) {
                        continue;
                    }
                }

                int num_skipped_elements = 0;
                MovePosition(second_set_elements, first_set_element, is_element_removed, &num_skipped_elements, &second_set_element_pos);
                *second_without_first_size += num_skipped_elements;
                const int second_set_element = (second_set_element_pos == static_cast<int>(second_set_elements.size())) ?
                                               num_elements : second_set_elements[second_set_element_pos];
                if (second_set_element == first_set_element) {
                    ++second_set_element_pos; // can become second_set_elements.size() + 1 on the last iteration
                } else {
                    ++(*first_without_second_size);
                }
            }
        }

        bool ReduceEdges(const bool make_simple_edge_reduction,
                         const Hypergraph& graph,
                         const std::vector<bool>& is_vertex_removed,
                         std::vector<bool>* is_edge_removed,
                         std::vector<int>* alive_edges) {
            assert(is_edge_removed);
            assert(alive_edges);

            std::vector<int> new_alive_edges;
            new_alive_edges.reserve(alive_edges->size());
            bool reduced = false;
            for (const int candidate : *alive_edges) {
                for (const int potential_dominator : *alive_edges) {
                    if (candidate == potential_dominator ||
                        (*is_edge_removed)[potential_dominator]) {
                        continue;
                    }

                    int dominator_without_candidate_size;
                    int candidate_without_dominator_size;
                    CalcSetDifferences(graph.edges[potential_dominator],
                                       graph.edges[candidate],
                                       is_vertex_removed,
                                       &dominator_without_candidate_size,
                                       &candidate_without_dominator_size);

                    const int candidate_demands = graph.edge_demands[candidate];
                    const int dominator_demands = graph.edge_demands[potential_dominator];

                    if (make_simple_edge_reduction) {
                        if (dominator_without_candidate_size > 0 ||
                            candidate_demands > dominator_demands) {
                            continue;
                        }
                        if (candidate_without_dominator_size == 0 &&
                            candidate_demands == dominator_demands &&
                            candidate < potential_dominator) {
                            continue;
                        }
                    } else {
                        if (candidate_demands == dominator_demands &&
                            dominator_without_candidate_size == 0 &&
                            candidate_without_dominator_size == 0 &&
                            candidate < potential_dominator) {
                            continue;
                        }
                        if (candidate_demands > dominator_demands - dominator_without_candidate_size) {
                            continue;
                        }
                    }

                    (*is_edge_removed)[candidate] = true;
                    reduced = true;
                    break;
                }

                if (!(*is_edge_removed)[candidate]) {
                    new_alive_edges.push_back(candidate);
                }
            }
            std::swap(*alive_edges, new_alive_edges);

            return reduced;
        }

        bool ReduceVertices(const Hypergraph& graph,
                            const std::vector<bool>& is_edge_removed,
                            std::vector<bool>* is_vertex_removed,
                            std::vector<int>* alive_vertices) {
            assert(is_vertex_removed);
            assert(alive_vertices);

            std::vector<int> new_alive_vertices;
            new_alive_vertices.reserve(alive_vertices->size());
            bool reduced = false;
            for (const int candidate : *alive_vertices) {
                int required_num_dominators = 0;
                for (const int edge : graph.vertices[candidate]) {
                    const int edge_demand = graph.edge_demands[edge];
                    if (!is_edge_removed[edge] && required_num_dominators < edge_demand) {
                        required_num_dominators = edge_demand;
                    }
                }

                int num_dominators = 0;
                for (const int potential_dominator : *alive_vertices) {
                    if (num_dominators == required_num_dominators) {
                        break;
                    }

                    if (candidate == potential_dominator ||
                        (*is_vertex_removed)[potential_dominator]) {
                        continue;
                    }

                    int dominator_without_candidate_size;
                    int candidate_without_dominator_size;
                    CalcSetDifferences(graph.vertices[potential_dominator],
                                       graph.vertices[candidate],
                                       is_edge_removed,
                                       &dominator_without_candidate_size,
                                       &candidate_without_dominator_size);

                    if (dominator_without_candidate_size == 0 &&
                        candidate_without_dominator_size == 0 &&
                        candidate < potential_dominator) {
                        continue;
                    }
                    if (candidate_without_dominator_size > 0) {
                        continue;
                    }

                    ++num_dominators;
                }

                if (num_dominators == required_num_dominators) {
                    (*is_vertex_removed)[candidate] = true;
                    reduced = true;
                }

                if (!(*is_vertex_removed)[candidate]) {
                    new_alive_vertices.push_back(candidate);
                }
            }
            std::swap(*alive_vertices, new_alive_vertices);

            return reduced;
        }
    }

    Hypergraph FWeiheNaiveReducer::Reduce(const Hypergraph& input,
                                          const int log_level,
                                          std::vector<int>* in_hitting_set,
                                          bool* is_reduced) const {
        assert(is_reduced);

        const int num_edges = input.edges.size();
        const int num_vertices = input.vertices.size();

        std::vector<int> alive_edges(num_edges);
        std::iota(alive_edges.begin(), alive_edges.end(), 0);
        std::vector<int> alive_vertices;
        alive_vertices.reserve(num_vertices);
        for (int vertex = 0; vertex < num_vertices; ++vertex) {
            if (!input.vertices[vertex].empty()) {
                alive_vertices.push_back(vertex);
            }
        }

        std::vector<bool> is_edge_removed(num_edges, false);
        std::vector<bool> is_vertex_removed(num_vertices, false);

        Hypergraph sorted_input = SortHypergraphEdgesAndVertices(input);

        bool first_iteration = true;
        while (true) {
            const bool edges_reduced = ReduceEdges(MakeSimpleEdgeReduction(),
                                                   sorted_input,
                                                   is_vertex_removed,
                                                   &is_edge_removed,
                                                   &alive_edges);
            if (!edges_reduced && !first_iteration) {
                break;
            }
            const bool vertices_reduced = ReduceVertices(sorted_input, is_edge_removed, &is_vertex_removed, &alive_vertices);
            if (!vertices_reduced) {
                break;
            }
        }

        *is_reduced = IsReduced(is_vertex_removed, is_edge_removed);

        return ConstructOutput(input.edges,
                               input.vertices.size(),
                               is_edge_removed,
                               is_vertex_removed,
                               input.edge_demands);
    }

    bool FWeiheNaiveReducer::MakeSimpleEdgeReduction() const {
        return false;
    }
}
