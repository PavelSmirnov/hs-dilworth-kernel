#include "gurobi_solver.h"
#include "common/log.h"
#ifdef USE_GUROBI
#include <gurobi_c++.h>
#endif

namespace fastweihe {
    bool GurobiSolverReducer::IsAvailable() const {
#ifdef USE_GUROBI
        return true;
#else
        return false;
#endif
    }

    Hypergraph GurobiSolverReducer::Reduce(const Hypergraph& input,
                                           const int log_level,
                                           std::vector<int>* in_hitting_set,
                                           bool* is_reduced) const {
        FASTWEIHE_INFO(log_level, "In Gurobi solver.\n");
#ifndef USE_GUROBI
        FASTWEIHE_ERROR(log_level, "fastweihe is built without gurobi, can't solve the instance with gurobi-solver.\n");
        *is_reduced = false;
        return input;
#else
        FASTWEIHE_INFO(log_level, "Setting Gurobi environment.\n");
        GRBEnv env = GRBEnv(true);
        env.set(GRB_IntParam_OutputFlag, 0);
        env.start();

        FASTWEIHE_INFO(log_level, "Creating Gurobi model.\n");
        GRBModel model = GRBModel(env);
        model.set(GRB_IntAttr_ModelSense, GRB_MINIMIZE);

        FASTWEIHE_INFO(log_level, "Adding Gurobi variables.\n");
        const int num_vertices = input.vertices.size();
        std::vector<double> ones(num_vertices, 1.0);
        std::vector<char> var_types(num_vertices, GRB_BINARY);
        GRBVar* vertex_vars = model.addVars(nullptr, // lower bounds, defaults to 0
                                            ones.data(), // upper bounds
                                            ones.data(), // objective coefficients
                                            var_types.data(),
                                            nullptr, // names
                                            num_vertices);

        FASTWEIHE_INFO(log_level, "Adding Gurobi constraints.\n");
        for (int edge = 0; edge < input.edges.size(); ++edge) {
            GRBLinExpr edge_constraint;
            for (const auto vertex : input.edges[edge]) {
                edge_constraint += *(vertex_vars + vertex);
            }
            model.addConstr(edge_constraint >= input.edge_demands[edge]);
        }

        FASTWEIHE_INFO(log_level, "Optimizing Gurobi model.\n");
        model.optimize();
        const auto status = model.get(GRB_IntAttr_Status);
        if (status != GRB_OPTIMAL) {
            FASTWEIHE_ERROR(log_level, "Optimization gone wrong, Gurobi status code: " << status << "\n");
        }

        FASTWEIHE_INFO(log_level, "Retrieving Gurobi solution.\n");
        for (int vertex = 0; vertex < num_vertices; ++vertex) {
            FASTWEIHE_DEBUG(log_level, "Checking vertex " << vertex << "...");
            if ((vertex_vars + vertex)->get(GRB_DoubleAttr_X) > 0.5) {
                FASTWEIHE_DEBUG(log_level, " In hitting set!\n");
                in_hitting_set->push_back(vertex);
            }
            FASTWEIHE_DEBUG(log_level, " NOT in hitting set.\n");
        }
        *is_reduced = input.edges.size() != 0;

        FASTWEIHE_DEBUG(log_level, "Deleting Gurobi variables.\n");
        delete vertex_vars;
        FASTWEIHE_DEBUG(log_level, "Returning solution.\n");
        return Hypergraph();
#endif
    }
}
