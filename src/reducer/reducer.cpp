#include "f_weihe_fast.h"
#include "f_weihe_fast_worse.h"
#include "f_weihe_opencl.h"
#include "f_weihe_naive.h"
#include "f_weihe_naive_worse.h"
#include "gurobi_solver.h"
#include "identity.h"
#include "reducer.h"
#include "remove_full_edges.h"
#include "weihe_fast.h"
#include "weihe_naive.h"
#include <cassert>
#include <memory>
#include <string>

namespace fastweihe {
    HittingSetReducer::~HittingSetReducer()
    {
    }

    bool HittingSetReducer::IsAvailable() const {
        return true;
    }

    std::unique_ptr<HittingSetReducer> MakeReducer(const std::string& name) {
        if (name == "identity") {
            return std::make_unique<IdentityReducer>();
        } else if (name == "weihe-fast") {
            return std::make_unique<WeiheFastReducer>();
        } else if (name == "f-weihe-fast") {
            return std::make_unique<FWeiheFastReducer>();
        } else if (name == "f-weihe-fast-worse") {
            return std::make_unique<FWeiheFastWorseReducer>();
        } else if (name == "weihe-naive") {
            return std::make_unique<WeiheNaiveReducer>();
        } else if (name == "f-weihe-naive") {
            return std::make_unique<FWeiheNaiveReducer>();
        } else if (name == "f-weihe-naive-worse") {
            return std::make_unique<FWeiheNaiveWorseReducer>();
        } else if (name == "remove-full-edges") {
            return std::make_unique<FullEdgesReducer>();
        } else if (name == "solve-gurobi") {
            return std::make_unique<GurobiSolverReducer>();
        } else if (name == "f-weihe-gpu-opencl") {
            return std::make_unique<FWeiheOpenCLReducer>();
        }
        assert(false);
        return nullptr;
    }
}
