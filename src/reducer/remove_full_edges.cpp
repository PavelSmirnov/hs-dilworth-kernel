#include "helpers.h"
#include "remove_full_edges.h"
#include "common/helpers.h"
#include "common/log.h"
#include <algorithm>
#include <cassert>
#include <vector>

namespace fastweihe {
    Hypergraph FullEdgesReducer::Reduce(const Hypergraph& input,
                                        const int log_level,
                                        std::vector<int>* in_hitting_set,
                                        bool* is_reduced) const {
        assert(is_reduced);

        FASTWEIHE_INFO(log_level, "\n"
                                  "------------------------------\n"
                                  "Starting full edges reduction.\n");
        FASTWEIHE_INFO(log_level, "Searching for full edges.\n");
        std::vector<int> new_edge_demands = input.edge_demands;
        std::vector<bool> is_edge_staged_for_removal(input.edges.size(), false);
        std::vector<bool> is_vertex_staged_for_removal(input.vertices.size(), false);
        for (int edge_number = 0; edge_number < static_cast<int>(input.edge_demands.size()); edge_number++) {
            FASTWEIHE_DEBUG(log_level, "On edge " << edge_number << ".");
            if (static_cast<int>(input.edges[edge_number].size()) == input.edge_demands[edge_number]) {
                FASTWEIHE_DEBUG(log_level, " Reduced!")
                is_edge_staged_for_removal[edge_number] = true;
                for (const int vertex_number : input.edges[edge_number]) {
                    is_vertex_staged_for_removal[vertex_number] = true;
                }
            }
            FASTWEIHE_DEBUG(log_level, "\n");
        }

        FASTWEIHE_INFO(log_level, "Reducing other edges demands.\n");
        for (int edge_number = 0; edge_number < static_cast<int>(input.edge_demands.size()); edge_number++) {
            if (is_edge_staged_for_removal[edge_number]) {
                continue;
            }

            FASTWEIHE_DEBUG(log_level, "On edge " << edge_number << ".\n");
            for (int vertex_number : input.edges[edge_number]) {
                new_edge_demands[edge_number] -= is_vertex_staged_for_removal[vertex_number];
            }
            if (new_edge_demands[edge_number] < 0) {
                new_edge_demands[edge_number] = 0;
            }
        }

        FASTWEIHE_INFO(log_level, "Adding vertices to hitting set.\n");
        for (int vertex_number = 0; vertex_number < static_cast<int>(input.vertices.size()); vertex_number++) {
            FASTWEIHE_DEBUG(log_level, "On vertex " << vertex_number << ".");
            if (is_vertex_staged_for_removal[vertex_number]) {
                FASTWEIHE_DEBUG(log_level, " In hitting set!");
                in_hitting_set->push_back(vertex_number);
            }
            FASTWEIHE_DEBUG(log_level, "\n");
        }

        FASTWEIHE_INFO(log_level, "Checking whether instance is reduced at all.\n");
        *is_reduced = IsReduced(is_vertex_staged_for_removal, is_edge_staged_for_removal);

        FASTWEIHE_INFO(log_level, "Constructing full edges reducer output.\n");
        return ConstructOutput(input.edges, input.vertices.size(), is_edge_staged_for_removal, is_vertex_staged_for_removal, new_edge_demands);
    }
}
