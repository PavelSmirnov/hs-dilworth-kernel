#pragma once
#include "reducer.h"

namespace fastweihe {
    class FWeiheOpenCLReducer : public HittingSetReducer {
    public:
        FWeiheOpenCLReducer(const int log_level = 1);
        ~FWeiheOpenCLReducer();
        Hypergraph Reduce(const Hypergraph& input,
                          const int log_level,
                          std::vector<int>* in_hitting_set,
                          bool* is_reduced) const override;

    private:
        const int _log_level;
    };
}
