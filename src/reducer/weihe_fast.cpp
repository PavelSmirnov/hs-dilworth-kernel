#include "helpers.h"
#include "weihe_fast.h"
#include "common/helpers.h"
#include <cassert>
#include <numeric>
#include <queue>
#include <vector>

namespace fastweihe {
    namespace {
        constexpr int NO_ITERATION = -1;

        struct IsDominatingEdge {
            bool operator()(const int potential_dominant_size, const int candidate_size) const {
                return candidate_size >= potential_dominant_size;
            }
        };

        struct IsDominatingVertex {
            bool operator()(const int potential_dominant_size, const int candidate_size) const {
                return candidate_size <= potential_dominant_size;
            }
        };

        template <class IsDominatingSet>
        void StageSetsForRemoval(const bool skip_empty,
                                 const IntersectionMatrix& set_intersection_sizes,
                                 const std::vector<int>& candidate_domain,
                                 const std::vector<int>& dominator_domain,
                                 const std::vector<int>& set_sizes,
                                 std::vector<bool>* is_set_staged_for_removal,
                                 std::queue<int>* sets_to_remove,
                                 const IsDominatingSet& is_dominating_set = IsDominatingSet()) {
            assert(is_set_staged_for_removal);
            assert(sets_to_remove);

            for (const auto candidate_set : candidate_domain) {
                const int candidate_size = set_sizes[candidate_set];
                for (const auto potential_dominant_set : dominator_domain) {
                    if ((*is_set_staged_for_removal)[potential_dominant_set] ||
                        candidate_set == potential_dominant_set) {
                        continue;
                    }
                    const int potential_dominant_size = set_sizes[potential_dominant_set];
                    const int intersection_size = set_intersection_sizes(candidate_set, potential_dominant_set);
                    if (intersection_size < candidate_size && intersection_size < potential_dominant_size) {
                        continue;
                    }
                    if (candidate_size == potential_dominant_size && candidate_set < potential_dominant_set) {
                        continue;
                    }
                    if (is_dominating_set(potential_dominant_size, candidate_size)) {
                        (*is_set_staged_for_removal)[candidate_set] = true;
                        sets_to_remove->push(candidate_set);
                        break;
                    }
                }
            }
        }

        template <class IsDominatingElement>
        void ReduceSets(const bool sets_are_edges,
                        const std::vector<std::vector<int>>& sets,
                        const std::vector<std::vector<int>>& elements,
                        const bool first_loop,
                        const int weihe_iteration,
                        std::vector<int>* last_element_affection_iteration,
                        std::vector<int>* alive_elements,
                        std::vector<int>* element_sizes,
                        std::vector<bool>* is_element_staged_for_removal,
                        IntersectionMatrix* element_intersection_sizes,
                        std::queue<int>* sets_to_remove,
                        std::queue<int>* elements_to_remove,
                        const IsDominatingElement& is_dominating_element = IsDominatingElement()) {
            assert(last_element_affection_iteration);
            assert(element_sizes);
            assert(is_element_staged_for_removal);
            assert(element_intersection_sizes);
            assert(sets_to_remove);
            assert(elements_to_remove);
            assert(alive_elements);

            std::vector<int> affected_elements;
            while (!sets_to_remove->empty()) {
                const auto removed_set = sets_to_remove->front();
                sets_to_remove->pop();
                const auto& set_elements = sets[removed_set];
                RemoveSet(set_elements, element_sizes, element_intersection_sizes);
                for (const int element : set_elements) {
                    if ((*is_element_staged_for_removal)[element]) {
                        continue;
                    }
                    auto& last_affection_iteration = (*last_element_affection_iteration)[element];
                    if (last_affection_iteration == weihe_iteration) {
                        continue;
                    }
                    last_affection_iteration = weihe_iteration;
                    affected_elements.push_back(element);
                }
            }

            if (first_loop) {
                affected_elements = *alive_elements;
            }

            const auto& candidate_domain = sets_are_edges ? affected_elements : *alive_elements;
            const auto& dominator_domain = sets_are_edges ? *alive_elements : affected_elements;

            StageSetsForRemoval(/*skip_empty*/sets_are_edges,
                                *element_intersection_sizes,
                                candidate_domain,
                                dominator_domain,
                                *element_sizes,
                                is_element_staged_for_removal,
                                elements_to_remove,
                                is_dominating_element);

            UpdateAliveElements(*is_element_staged_for_removal, alive_elements);
        }
    }

    Hypergraph WeiheFastReducer::Reduce(const Hypergraph& input,
                                        const int log_level,
                                        std::vector<int>* in_hitting_set,
                                        bool* is_reduced) const {
        assert(is_reduced);

        const int num_edges = input.edges.size();
        const int num_vertices = input.vertices.size();

        std::vector<int> edge_sizes = CalcSetSizes(input.edges);
        std::vector<int> vertex_sizes = CalcSetSizes(input.vertices);

        auto edge_intersection_sizes = InitializeSetIntersectionSizes(input.edges, input.vertices);
        auto vertex_intersection_sizes = InitializeSetIntersectionSizes(input.vertices, input.edges);

        std::vector<int> alive_edges(num_edges);
        std::iota(alive_edges.begin(), alive_edges.end(), 0);
        std::vector<int> alive_vertices;
        alive_vertices.reserve(num_vertices);
        for (int vertex = 0; vertex < num_vertices; ++vertex) {
            if (!input.vertices[vertex].empty()) {
                alive_vertices.push_back(vertex);
            }
        }

        std::vector<int> last_edge_affection_iteration(num_edges, NO_ITERATION);
        std::vector<int> last_vertex_affection_iteration(num_vertices, NO_ITERATION);

        std::vector<bool> is_edge_staged_for_removal(num_edges, false);
        std::vector<bool> is_vertex_staged_for_removal(num_vertices, false);
        std::queue<int> edges_to_remove;
        std::queue<int> vertices_to_remove;

        bool first_loop = true;
        int weihe_iteration = 0;
        while (true) {
            ReduceSets<IsDominatingEdge>(/*sets_are_edges*/false,
                                         input.vertices,
                                         input.edges,
                                         first_loop,
                                         weihe_iteration,
                                         &last_edge_affection_iteration,
                                         &alive_edges,
                                         &edge_sizes,
                                         &is_edge_staged_for_removal,
                                         &edge_intersection_sizes,
                                         &vertices_to_remove,
                                         &edges_to_remove);
            if (edges_to_remove.empty() && !first_loop) {
                break;
            }

            ++weihe_iteration;

            ReduceSets<IsDominatingVertex>(/*sets_are_edges*/true,
                                           input.edges,
                                           input.vertices,
                                           first_loop,
                                           weihe_iteration,
                                           &last_vertex_affection_iteration,
                                           &alive_vertices,
                                           &vertex_sizes,
                                           &is_vertex_staged_for_removal,
                                           &vertex_intersection_sizes,
                                           &edges_to_remove,
                                           &vertices_to_remove);
            if (vertices_to_remove.empty()) {
                break;
            }
            first_loop = false;
        }

        *is_reduced = IsReduced(is_vertex_staged_for_removal, is_edge_staged_for_removal);

        return ConstructOutput(input.edges,
                               num_vertices,
                               is_edge_staged_for_removal,
                               is_vertex_staged_for_removal,
                               input.edge_demands);
    }
}
