#pragma once
#include "common/hypergraph.h"
#include <memory>
#include <string>
#include <vector>

namespace fastweihe {
    class HittingSetReducer {
    public:
        virtual Hypergraph Reduce(const Hypergraph& input,
                                  const int log_level,
                                  std::vector<int>* in_hitting_set,
                                  bool* is_reduced) const = 0;
        virtual bool IsAvailable() const;
        virtual ~HittingSetReducer();
    };

    std::unique_ptr<HittingSetReducer> MakeReducer(const std::string& name);
}
