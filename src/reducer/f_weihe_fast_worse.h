#pragma once
#include "f_weihe_fast.h"

namespace fastweihe {
    class FWeiheFastWorseReducer : public FWeiheFastReducer {
    protected:
        bool MakeSimpleEdgeReduction() const override;
    };
}
