#include "identity.h"
#include <cassert>

namespace fastweihe {
    Hypergraph IdentityReducer::Reduce(const Hypergraph& input,
                                       const int log_level,
                                       std::vector<int>* in_hitting_set,
                                       bool* is_reduced) const {
        assert(is_reduced);

        *is_reduced = false;
        return input;
    }
}
