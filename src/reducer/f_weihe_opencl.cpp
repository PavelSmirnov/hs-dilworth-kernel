#include "f_weihe_opencl.h"
#include "helpers.h"
#include "common/helpers.h"
#include "common/log.h"
#include <cassert>
#include <chrono>
#include <ctime>
#include <numeric>
#include <type_traits>
#ifdef USE_OPENCL
#define CL_TARGET_OPENCL_VERSION 220
#include <CL/cl.h>
#endif

#ifndef USE_OPENCL

namespace fastweihe {
    FWeiheOpenCLReducer::FWeiheOpenCLReducer(const int log_level)
        : _log_level(log_level) {}
    FWeiheOpenCLReducer::~FWeiheOpenCLReducer() {}
    Hypergraph FWeiheOpenCLReducer::Reduce(const Hypergraph& input,
                                        const int log_level,
                                        std::vector<int>* in_hitting_set,
                                        bool* is_reduced) const {
        FASTWEIHE_ERROR(log_level, "fastweihe is built without opencl, can't solve the instance with f-weihe-gpu-opencl.\n");
        *is_reduced = false;
        return input;
    }
}

#else

#define CL_ENSURE(log_level, ret) { if (ret != CL_SUCCESS) { FASTWEIHE_ERROR(log_level, "OpenCL failed with error code " << ret << std::endl); exit(1); } }

namespace fastweihe {
    namespace {
        using PackingType = unsigned int;

        constexpr size_t TILE_SIZE = 8;
        constexpr size_t PACK_SIZE = 32;

        const int ZERO = 0;
        const char ONE_CHAR = 1;

        int NUM_OPENCL_INITIALIZATIONS = 0;
        cl_device_id DEVICE_ID;
        cl_context CONTEXT;
        cl_command_queue COMMAND_QUEUE;
        cl_program PROGRAM;
        cl_kernel REDUCE_VERTICES_KERNEL;
        cl_kernel REDUCE_EDGES_KERNEL;

        const char PROGRAM_SRC[] = R"abc(
#define TILE_SIZE 8
#define PACK_SIZE 32
#define PACKING_TYPE unsigned int

__kernel void reduce_edges(const int num_edges,
                           const int num_vertices,
                           __global const PACKING_TYPE* graph_tr,
                           __global const int* edge_demands,
                           __global unsigned char* is_edge_alive_out,
                           const int num_alive_vertices,
                           const int num_alive_edges) {
    const int i = get_global_id(0); // result matrix i
    const int j = get_global_id(1); // result matrix j
    const int local_i = get_local_id(0);
    const int local_j = get_local_id(1);
    const int tile_a_start = i - local_i;
    const int tile_b_start = j - local_j;

    const bool is_i_alive = i < num_alive_edges;
    const bool is_j_alive = j < num_alive_edges;

    int intersection = 0;
    __local int i_length_parts[TILE_SIZE][TILE_SIZE];
    __local int j_length_parts[TILE_SIZE][TILE_SIZE];
    i_length_parts[local_i][local_j] = 0;
    j_length_parts[local_i][local_j] = 0;
    __local unsigned char tile_a[TILE_SIZE][TILE_SIZE];
    __local unsigned char tile_b[TILE_SIZE][TILE_SIZE];
    for (int tile_k = 0; tile_k < num_alive_vertices; tile_k += TILE_SIZE) {
        const bool is_vertex_alive_tile_local_j = tile_k + local_j < num_alive_vertices;

        const bool is_edge_alive_tile_a_local_i = tile_a_start + local_i < num_alive_edges;
        if (is_edge_alive_tile_a_local_i && is_vertex_alive_tile_local_j) {
            const int edge = (tile_a_start + local_i);
            const int vertex = tile_k + local_j;
            tile_a[local_i][local_j] = (graph_tr[(edge / PACK_SIZE) * num_vertices + vertex] >> (edge & (PACK_SIZE - 1))) & 1;
        } else {
            tile_a[local_i][local_j] = 0;
        }
        const bool is_edge_alive_tile_b_local_i = tile_b_start + local_i < num_alive_edges;
        if (is_edge_alive_tile_b_local_i && is_vertex_alive_tile_local_j) {
            const int edge = (tile_b_start + local_i);
            const int vertex = tile_k + local_j;
            tile_b[local_i][local_j] = (graph_tr[(edge / PACK_SIZE) * num_vertices + vertex] >> (edge & (PACK_SIZE - 1))) & 1;
        } else {
            tile_b[local_i][local_j] = 0;
        }

        barrier(CLK_LOCAL_MEM_FENCE);

        if (is_i_alive && is_j_alive) {
            for (int k = 0; k < TILE_SIZE; ++k) {
                intersection += tile_a[local_i][k] * tile_b[local_j][k];
            }
        }
        i_length_parts[local_i][local_j] += tile_a[local_i][local_j]; // TODO: can to in a separate run? is it better?
        j_length_parts[local_i][local_j] += tile_b[local_i][local_j]; // TODO: can to in a separate run? is it better?

        barrier(CLK_LOCAL_MEM_FENCE);
    }

    __local int i_edge_length[TILE_SIZE];
    __local int j_edge_length[TILE_SIZE];
    if (local_i == 0) {
        i_edge_length[local_j] = 0;
        j_edge_length[local_j] = 0;
        for (int k = 0; k < TILE_SIZE; ++k) {
            i_edge_length[local_j] += i_length_parts[local_j][k]; // TODO: can optimize banks? worth it? probably not if TILE SIZE < num_vertices / TILE_SIZE
            j_edge_length[local_j] += j_length_parts[local_j][k]; // TODO: can optimize banks? worth it? probably not if TILE SIZE < num_vertices / TILE_SIZE
        }
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    if (!is_i_alive) {
        is_edge_alive_out[i] = 0;
    } else if (is_j_alive) {
        int i_without_j = i_edge_length[local_i] - intersection;
        int j_without_i = j_edge_length[local_j] - intersection;
        int demands_difference = edge_demands[i] - edge_demands[j];

        int i_dominates_j = demands_difference >= i_without_j;
        int j_dominates_i = -demands_difference >= j_without_i;
        int is_dominated = j_dominates_i && (!i_dominates_j || i > j);

        if (is_dominated) {
            is_edge_alive_out[i] = 0;
        }
    }
}

__kernel void reduce_vertices(const int num_edges,
                              const int num_vertices,
                              __global const PACKING_TYPE* graph_tr,
                              __global const int* edge_demands,
                              __global unsigned char* is_vertex_alive_out,
                              volatile __global int* num_vertex_dominators,
                              const int num_alive_vertices,
                              const int num_alive_edges) {
    const int i = get_global_id(0);
    const int j = get_global_id(1);
    const int local_i = get_local_id(0);
    const int local_j = get_local_id(1);
    const int tile_a_start = i - local_i;
    const int tile_b_start = j - local_j;

    int intersection = 0;
    __local int i_length_parts[TILE_SIZE];
    __local int j_length_parts[TILE_SIZE];
    __local int i_required_dominators_parts[PACK_SIZE][TILE_SIZE];

    if (local_i == 0) {
        i_length_parts[local_j] = 0;
        j_length_parts[local_j] = 0;
    }
    for (int k = local_i; k < PACK_SIZE; k += TILE_SIZE) {
        i_required_dominators_parts[k][local_j] = 0;
    }

    const bool is_i_alive = i < num_alive_vertices;
    const bool is_j_alive = j < num_alive_vertices;

    __local PACKING_TYPE tile_a[TILE_SIZE];
    __local PACKING_TYPE tile_b[TILE_SIZE];
    __local int tile_edge_demands[PACK_SIZE];
    for (int tile_k = 0; tile_k < num_edges / PACK_SIZE; ++tile_k) {
        int tile_k_expanded = tile_k * PACK_SIZE;

        PACKING_TYPE is_tile_alive = 0;
        for (int k = 0; k < PACK_SIZE; ++k) {
            const PACKING_TYPE is_edge_alive = tile_k_expanded + k < num_alive_edges;
            is_tile_alive |= (is_edge_alive << k);
        }

        if (local_i == 0) {
            const bool is_vertex_alive_tile_a_local_j = tile_a_start + local_j < num_alive_vertices;
            if (is_vertex_alive_tile_a_local_j) {
                PACKING_TYPE is_connected = graph_tr[tile_k * num_vertices + (tile_a_start + local_j)];
                tile_a[local_j] = is_connected & is_tile_alive;
            } else {
                tile_a[local_j] = 0;
            }
            const bool is_vertex_alive_tile_b_local_j = tile_b_start + local_j < num_alive_vertices;
            if (is_vertex_alive_tile_b_local_j) {
                PACKING_TYPE is_connected = graph_tr[tile_k * num_vertices + (tile_b_start + local_j)];
                tile_b[local_j] = is_connected & is_tile_alive;
            } else {
                tile_b[local_j] = 0;
            }
        }

        if (local_i < PACK_SIZE / TILE_SIZE) {
            tile_edge_demands[local_j + local_i * TILE_SIZE] = edge_demands[tile_k_expanded + local_j + local_i * TILE_SIZE];
        }

        barrier(CLK_LOCAL_MEM_FENCE);

        for (int k = local_i; k < PACK_SIZE; k += TILE_SIZE) {
            if (tile_a[local_j] & (((PACKING_TYPE)1) << k)) {
                i_required_dominators_parts[k][local_j] = max(i_required_dominators_parts[k][local_j], tile_edge_demands[k]);
            }
        }
        if (is_i_alive && is_j_alive) {
            intersection += popcount(tile_a[local_i] & tile_b[local_j]);
        }
        if (local_i == 0) {
            i_length_parts[local_j] += popcount(tile_a[local_j]);
            j_length_parts[local_j] += popcount(tile_b[local_j]);
        }

        barrier(CLK_LOCAL_MEM_FENCE);
    }

    int i_vertex_length = i_length_parts[local_i];
    int j_vertex_length = j_length_parts[local_j];
    int i_required_dominators = 0;
    for (int k = 0; k < PACK_SIZE; ++k) {
        i_required_dominators = max(i_required_dominators, i_required_dominators_parts[k][local_i]);
    }

    if (!is_i_alive) {
        is_vertex_alive_out[i] = 0;
    } else if (i_required_dominators == 0) {
        is_vertex_alive_out[i] = 0;
    } else if (is_j_alive) {
        int i_dominates_j = j_vertex_length == intersection;
        int j_dominates_i = i_vertex_length == intersection;
        int is_dominated = j_dominates_i && (!i_dominates_j || i > j);

        if (is_dominated) {
            int num_dominators_so_far = atomic_inc(&num_vertex_dominators[i]) + 1;
            if (num_dominators_so_far >= i_required_dominators) {
                is_vertex_alive_out[i] = 0;
            }
        }
    }
}
        )abc";
    }

    FWeiheOpenCLReducer::FWeiheOpenCLReducer(const int log_level)
        : _log_level(log_level) {
        ++NUM_OPENCL_INITIALIZATIONS;
        if (NUM_OPENCL_INITIALIZATIONS > 1) {
            return;
        }
        cl_platform_id platform_id = NULL;
        cl_uint ret_num_platforms;
        CL_ENSURE(_log_level, clGetPlatformIDs(1, &platform_id, &ret_num_platforms));

        cl_uint ret_num_devices;
        CL_ENSURE(_log_level, clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_GPU, 1, &DEVICE_ID, &ret_num_devices));

        cl_int ret;
        CONTEXT = clCreateContext(NULL, 1, &DEVICE_ID, NULL, NULL, &ret);
        CL_ENSURE(_log_level, ret);

        COMMAND_QUEUE = clCreateCommandQueue(CONTEXT, DEVICE_ID, 0, &ret);
        CL_ENSURE(_log_level, ret);

        const char* programs_srcs = PROGRAM_SRC;
        const size_t program_size = sizeof(PROGRAM_SRC);
        PROGRAM = clCreateProgramWithSource(CONTEXT, 1, &programs_srcs, &program_size, &ret);
        CL_ENSURE(_log_level, ret);
        CL_ENSURE(_log_level, clBuildProgram(PROGRAM, 1, &DEVICE_ID, NULL, NULL, NULL));

        REDUCE_VERTICES_KERNEL = clCreateKernel(PROGRAM, "reduce_vertices", &ret);
        CL_ENSURE(_log_level, ret);
        REDUCE_EDGES_KERNEL = clCreateKernel(PROGRAM, "reduce_edges", &ret);
        CL_ENSURE(_log_level, ret);
    }

    FWeiheOpenCLReducer::~FWeiheOpenCLReducer() {
        --NUM_OPENCL_INITIALIZATIONS;
        if (NUM_OPENCL_INITIALIZATIONS > 0) {
            return;
        }
        CL_ENSURE(_log_level, clReleaseKernel(REDUCE_VERTICES_KERNEL));
        CL_ENSURE(_log_level, clReleaseKernel(REDUCE_EDGES_KERNEL));
        CL_ENSURE(_log_level, clReleaseProgram(PROGRAM));
        CL_ENSURE(_log_level, clReleaseCommandQueue(COMMAND_QUEUE));
        CL_ENSURE(_log_level, clReleaseContext(CONTEXT));
    }

    Hypergraph FWeiheOpenCLReducer::Reduce(const Hypergraph& input,
                                           const int log_level,
                                           std::vector<int>* in_hitting_set,
                                           bool* is_reduced) const {
        assert(in_hitting_set);
        assert(is_reduced);

        cl_int ret;

        // prepare vertex maintainance
        const int num_vertices = std::max<int>(1, (input.vertices.size() + 1 + TILE_SIZE - 1) / TILE_SIZE) * TILE_SIZE;
        std::vector<int> alive_vertices_list(num_vertices, -1);
        cl_int num_alive_vertices = 0;
        for (int vertex_id = 0; vertex_id < input.vertices.size(); ++vertex_id) {
            if (!input.vertices[vertex_id].empty()) {
                alive_vertices_list[num_alive_vertices++] = vertex_id;
            }
        }
        cl_int padded_num_alive_vertices = (std::max<int>(1, num_alive_vertices) + TILE_SIZE - 1) / TILE_SIZE * TILE_SIZE;
        std::vector<bool> is_vertex_staged_for_removal(input.vertices.size(), 0);

        // prepare edge maintainance
        const int num_edges = std::max<int>(1, (input.edges.size() + PACK_SIZE - 1) / PACK_SIZE) * PACK_SIZE;
        std::vector<int> alive_edges_list(num_edges, -1);
        std::vector<int> edge_id_to_compressed(input.edges.size());
        cl_int num_alive_edges = 0;
        for (int edge_id = 0; edge_id < input.edges.size(); ++edge_id) {
            edge_id_to_compressed[edge_id] = num_alive_edges;
            alive_edges_list[num_alive_edges++] = edge_id;
        }
        cl_int padded_num_alive_edges = (std::max<int>(1, num_alive_edges) + PACK_SIZE - 1) / PACK_SIZE * PACK_SIZE;
        std::vector<int> edge_demands_cpu(padded_num_alive_edges);
        std::vector<bool> is_edge_staged_for_removal(input.edges.size(), 0);

        // allocate buffers
        cl_mem graph_gpu_tr = clCreateBuffer(CONTEXT, CL_MEM_READ_ONLY, padded_num_alive_edges * padded_num_alive_vertices / PACK_SIZE * sizeof(PackingType), nullptr, &ret);
        CL_ENSURE(log_level, ret);
        cl_mem is_edge_alive_gpu = clCreateBuffer(CONTEXT, CL_MEM_READ_WRITE, padded_num_alive_edges * sizeof(unsigned char), nullptr, &ret);
        CL_ENSURE(log_level, ret);
        cl_mem is_vertex_alive_gpu = clCreateBuffer(CONTEXT, CL_MEM_READ_WRITE, padded_num_alive_vertices * sizeof(unsigned char), nullptr, &ret);
        CL_ENSURE(log_level, ret);
        cl_mem edge_demands_gpu = clCreateBuffer(CONTEXT, CL_MEM_READ_ONLY, padded_num_alive_edges * sizeof(int), nullptr, &ret);
        CL_ENSURE(log_level, ret);
        cl_mem num_vertex_dominators_gpu = clCreateBuffer(CONTEXT, CL_MEM_READ_WRITE, sizeof(int) * padded_num_alive_vertices, nullptr, &ret);
        CL_ENSURE(log_level, ret);
        clEnqueueBarrierWithWaitList(COMMAND_QUEUE, 0, NULL, NULL);

        // run cycle
        *is_reduced = false;
        for (bool is_first_iteration = true;; is_first_iteration = false) {
            // update graph
            std::vector<PackingType> graph_cpu_tr(padded_num_alive_edges * padded_num_alive_vertices / PACK_SIZE, 0);
            for (int compressed_vertex_id = 0; compressed_vertex_id < num_alive_vertices; ++compressed_vertex_id) {
                for (const auto edge_id : input.vertices[alive_vertices_list[compressed_vertex_id]]) {
                    if (is_edge_staged_for_removal[edge_id]) {
                        continue;
                    }
                    const int compressed_edge_id = edge_id_to_compressed[edge_id];
                    graph_cpu_tr[compressed_edge_id / PACK_SIZE * padded_num_alive_vertices + compressed_vertex_id] |= (PackingType(1) << (compressed_edge_id & (PACK_SIZE - 1)));
                }
            }
            CL_ENSURE(log_level, clEnqueueWriteBuffer(COMMAND_QUEUE, graph_gpu_tr, CL_TRUE, 0, graph_cpu_tr.size() * sizeof(PackingType), graph_cpu_tr.data(), 0, NULL, NULL));

            // update edge demands
            if (is_first_iteration) {
                for (int compressed_edge_id = 0; compressed_edge_id < num_alive_edges; ++compressed_edge_id) {
                    edge_demands_cpu[compressed_edge_id] = input.edge_demands[alive_edges_list[compressed_edge_id]];
                }
                if (num_alive_edges > 0) {
                    CL_ENSURE(log_level, clEnqueueWriteBuffer(COMMAND_QUEUE, edge_demands_gpu, CL_TRUE, 0, num_alive_edges * sizeof(int), edge_demands_cpu.data(), 0, NULL, NULL));
                }
            }

            // prepare the result buffer
            if (num_alive_edges > 0) {
                CL_ENSURE(log_level, clEnqueueFillBuffer(COMMAND_QUEUE, is_edge_alive_gpu, (const void*)&ONE_CHAR, sizeof(char), 0, num_alive_edges * sizeof(char), 0, NULL, NULL));
            }

            clEnqueueBarrierWithWaitList(COMMAND_QUEUE, 0, NULL, NULL);

            // run edge reduction
            CL_ENSURE(log_level, clSetKernelArg(REDUCE_EDGES_KERNEL, 0, sizeof(cl_int), &padded_num_alive_edges));
            CL_ENSURE(log_level, clSetKernelArg(REDUCE_EDGES_KERNEL, 1, sizeof(cl_int), &padded_num_alive_vertices));
            CL_ENSURE(log_level, clSetKernelArg(REDUCE_EDGES_KERNEL, 2, sizeof(cl_mem), (void*)&graph_gpu_tr));
            CL_ENSURE(log_level, clSetKernelArg(REDUCE_EDGES_KERNEL, 3, sizeof(cl_mem), (void*)&edge_demands_gpu));
            CL_ENSURE(log_level, clSetKernelArg(REDUCE_EDGES_KERNEL, 4, sizeof(cl_mem), (void*)&is_edge_alive_gpu));
            CL_ENSURE(log_level, clSetKernelArg(REDUCE_EDGES_KERNEL, 5, sizeof(cl_int), &num_alive_vertices));
            CL_ENSURE(log_level, clSetKernelArg(REDUCE_EDGES_KERNEL, 6, sizeof(cl_int), &num_alive_edges));
            size_t edge_global_work_size[] = {static_cast<size_t>(padded_num_alive_edges), static_cast<size_t>(padded_num_alive_edges)};
            size_t edge_local_work_size[] = {TILE_SIZE, TILE_SIZE};
            CL_ENSURE(log_level, clEnqueueNDRangeKernel(COMMAND_QUEUE, REDUCE_EDGES_KERNEL, 2, NULL, edge_global_work_size, edge_local_work_size, 0, NULL, NULL));

            clEnqueueBarrierWithWaitList(COMMAND_QUEUE, 0, NULL, NULL);

            // read the edge reduction result
            std::vector<char> is_edge_alive_cpu(num_alive_edges);
            if (num_alive_edges > 0) {
                CL_ENSURE(log_level, clEnqueueReadBuffer(COMMAND_QUEUE, is_edge_alive_gpu, CL_TRUE, 0, num_alive_edges * sizeof(char), is_edge_alive_cpu.data(), 0, NULL, NULL));
            }
            CL_ENSURE(log_level, clFlush(COMMAND_QUEUE));
            CL_ENSURE(log_level, clFinish(COMMAND_QUEUE));
            bool is_reduced_edges = false;
            int new_num_alive_edges = 0;
            for (int i = 0; i < num_alive_edges; ++i) {
                const int edge_id = alive_edges_list[i];
                if (is_edge_alive_cpu[i]) {
                    edge_id_to_compressed[edge_id] = new_num_alive_edges;
                    alive_edges_list[new_num_alive_edges++] = edge_id;
                } else {
                    is_edge_staged_for_removal[edge_id] = true;
                    is_reduced_edges = true;
                }
            }
            num_alive_edges = new_num_alive_edges;
            padded_num_alive_edges = (std::max(1, new_num_alive_edges) + PACK_SIZE - 1) / PACK_SIZE * PACK_SIZE;
            *is_reduced = *is_reduced || is_reduced_edges;
            if (!is_reduced_edges && !is_first_iteration) {
                break;
            }

            // update graph
            graph_cpu_tr.assign(padded_num_alive_edges * padded_num_alive_vertices / PACK_SIZE, 0);
            for (int compressed_vertex_id = 0; compressed_vertex_id < num_alive_vertices; ++compressed_vertex_id) {
                for (const auto edge_id : input.vertices[alive_vertices_list[compressed_vertex_id]]) {
                    if (is_edge_staged_for_removal[edge_id]) {
                        continue;
                    }
                    const int compressed_edge_id = edge_id_to_compressed[edge_id];
                    graph_cpu_tr[compressed_edge_id / PACK_SIZE * padded_num_alive_vertices + compressed_vertex_id] |= (PackingType(1) << (compressed_edge_id & (PACK_SIZE - 1)));
                }
            }
            CL_ENSURE(log_level, clEnqueueWriteBuffer(COMMAND_QUEUE, graph_gpu_tr, CL_TRUE, 0, graph_cpu_tr.size() * sizeof(PackingType), graph_cpu_tr.data(), 0, NULL, NULL));

            // update edge demands
            for (int compressed_edge_id = 0; compressed_edge_id < num_alive_edges; ++compressed_edge_id) {
                edge_demands_cpu[compressed_edge_id] = input.edge_demands[alive_edges_list[compressed_edge_id]];
            }
            if (num_alive_edges > 0) {
                CL_ENSURE(log_level, clEnqueueWriteBuffer(COMMAND_QUEUE, edge_demands_gpu, CL_TRUE, 0, num_alive_edges * sizeof(int), edge_demands_cpu.data(), 0, NULL, NULL));
            }

            // prepare the result buffers
            if (num_alive_vertices > 0) {
                CL_ENSURE(log_level, clEnqueueFillBuffer(COMMAND_QUEUE, num_vertex_dominators_gpu, (const void*)&ZERO, sizeof(int), 0, num_alive_vertices * sizeof(int), 0, NULL, NULL));
                CL_ENSURE(log_level, clEnqueueFillBuffer(COMMAND_QUEUE, is_vertex_alive_gpu, (const void*)&ONE_CHAR, sizeof(char), 0, num_alive_vertices * sizeof(char), 0, NULL, NULL));
            }

            clEnqueueBarrierWithWaitList(COMMAND_QUEUE, 0, NULL, NULL);

            // run vertex reduction kernel
            CL_ENSURE(log_level, clSetKernelArg(REDUCE_VERTICES_KERNEL, 0, sizeof(cl_int), &padded_num_alive_edges));
            CL_ENSURE(log_level, clSetKernelArg(REDUCE_VERTICES_KERNEL, 1, sizeof(cl_int), &padded_num_alive_vertices));
            CL_ENSURE(log_level, clSetKernelArg(REDUCE_VERTICES_KERNEL, 2, sizeof(cl_mem), (void*)&graph_gpu_tr));
            CL_ENSURE(log_level, clSetKernelArg(REDUCE_VERTICES_KERNEL, 3, sizeof(cl_mem), (void*)&edge_demands_gpu));
            CL_ENSURE(log_level, clSetKernelArg(REDUCE_VERTICES_KERNEL, 4, sizeof(cl_mem), (void*)&is_vertex_alive_gpu));
            CL_ENSURE(log_level, clSetKernelArg(REDUCE_VERTICES_KERNEL, 5, sizeof(cl_mem), (void*)&num_vertex_dominators_gpu));
            CL_ENSURE(log_level, clSetKernelArg(REDUCE_VERTICES_KERNEL, 6, sizeof(cl_int), &num_alive_vertices));
            CL_ENSURE(log_level, clSetKernelArg(REDUCE_VERTICES_KERNEL, 7, sizeof(cl_int), &num_alive_edges));
            size_t vertex_global_work_size[] = {static_cast<size_t>(padded_num_alive_vertices), static_cast<size_t>(padded_num_alive_vertices)};
            size_t vertex_local_work_size[] = {TILE_SIZE, TILE_SIZE};
            CL_ENSURE(log_level, clEnqueueNDRangeKernel(COMMAND_QUEUE, REDUCE_VERTICES_KERNEL, 2, NULL, vertex_global_work_size, vertex_local_work_size, 0, NULL, NULL));

            clEnqueueBarrierWithWaitList(COMMAND_QUEUE, 0, NULL, NULL);

            // read the vertex reduction result
            std::vector<char> is_vertex_alive_cpu(num_alive_vertices);
            if (num_alive_vertices > 0) {
                CL_ENSURE(log_level, clEnqueueReadBuffer(COMMAND_QUEUE, is_vertex_alive_gpu, CL_TRUE, 0, num_alive_vertices * sizeof(char), is_vertex_alive_cpu.data(), 0, NULL, NULL));
            }
            CL_ENSURE(log_level, clFlush(COMMAND_QUEUE));
            CL_ENSURE(log_level, clFinish(COMMAND_QUEUE));
            bool is_reduced_vertices = false;
            int new_num_alive_vertices = 0;
            for (int i = 0; i < num_alive_vertices; ++i) {
                if (is_vertex_alive_cpu[i]) {
                    alive_vertices_list[new_num_alive_vertices++] = alive_vertices_list[i];
                } else {
                    is_vertex_staged_for_removal[alive_vertices_list[i]] = true;
                    is_reduced_vertices = true;
                }
            }
            num_alive_vertices = new_num_alive_vertices;
            padded_num_alive_vertices = (std::max(1, new_num_alive_vertices) + TILE_SIZE - 1) / TILE_SIZE * TILE_SIZE;
            *is_reduced = *is_reduced || is_reduced_vertices;
            if (!is_reduced_vertices) {
                break;
            }
        }

        // release buffers
        CL_ENSURE(_log_level, clReleaseMemObject(is_edge_alive_gpu));
        CL_ENSURE(_log_level, clReleaseMemObject(is_vertex_alive_gpu));
        CL_ENSURE(_log_level, clReleaseMemObject(graph_gpu_tr));
        CL_ENSURE(_log_level, clReleaseMemObject(edge_demands_gpu));
        CL_ENSURE(_log_level, clReleaseMemObject(num_vertex_dominators_gpu));

        return ConstructOutput(input.edges,
                               input.vertices.size(),
                               is_edge_staged_for_removal,
                               is_vertex_staged_for_removal,
                               input.edge_demands);
    }
}

#endif
