#pragma once
#include "common/hypergraph.h"
#include <cstdint>
#include <stdexcept>
#include <vector>

namespace fastweihe {
    class IntersectionMatrix {
    public:
        using Type = uint16_t;

        IntersectionMatrix(const int size);

        inline void Inc(const int i, const int j) {
            Type& element = (i > j) ? _data[j][i - j] : _data[i][j - i];
            if (element == UINT16_MAX) {
                throw std::overflow_error("max intersection size reached");
            }
            ++element;
        }

        inline void Dec(const int i, const int j) {
            --((i > j) ? _data[j][i - j] : _data[i][j - i]);
        }

        inline const Type& operator()(const int i, const int j) const {
            return (i > j) ? _data[j][i - j] : _data[i][j - i];
        }

    private:
        std::vector<std::vector<Type>> _data;
    };

    std::vector<int> CalcSetSizes(const std::vector<std::vector<int>>& sets);

    IntersectionMatrix InitializeSetIntersectionSizes(const std::vector<std::vector<int>>& sets,
                                                      const std::vector<std::vector<int>>& elements);

    void RemoveSet(const std::vector<int>& element_set,
                   std::vector<int>* element_sizes,
                   IntersectionMatrix* element_intersection_sizes);

    Hypergraph ConstructOutput(const std::vector<std::vector<int>>& edges,
                               const int num_vertices,
                               const std::vector<bool>& is_edge_staged_for_removal,
                               const std::vector<bool>& is_vertex_staged_for_removal,
                               const std::vector<int>& edge_demands);

    bool IsReduced(const std::vector<bool>& is_vertex_removed, const std::vector<bool>& is_edge_removed);

    void AddHittingSetData(const std::vector<int>& src, std::vector<int>* dest);

    bool AreHypergraphsEqual(const Hypergraph& first, const Hypergraph& second);

    void UpdateAliveElements(const std::vector<bool>& is_element_alive, std::vector<int>* alive_elements);
}
