#pragma once
#include "estimator.h"
#include "common/hypergraph.h"

namespace fastweihe {
    class MaxDemandLowerBoundEstimator : public LowerBoundEstimator {
    public:
        int EstimateLowerBound(const Hypergraph& input, const int log_level) const override;
    };
}
