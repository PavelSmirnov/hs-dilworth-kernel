#include "estimator.h"
#include "max_demand.h"
#include "trivial.h"
#include <cassert>
#include <memory>
#include <string>

namespace fastweihe {
    LowerBoundEstimator::~LowerBoundEstimator()
    {
    }

    std::unique_ptr<LowerBoundEstimator> MakeLowerBoundEstimator(const std::string& name) {
        if (name == "trivial") {
            return std::make_unique<TrivialLowerBoundEstimator>();
        } else if (name == "max-demand") {
            return std::make_unique<MaxDemandLowerBoundEstimator>();
        }
        assert(false);
        return nullptr;
    }
}
