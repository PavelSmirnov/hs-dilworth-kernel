#include "common/helpers.h"
#include "common/hypergraph.h"
#include <algorithm>

namespace fastweihe {
    Hypergraph ConstructPushThroughHypergraph(const Hypergraph& hypergraph,
                                              const int pivot_edge,
                                              const std::vector<int>& removed_edges_in_order) {
        const int num_edges = hypergraph.edges.size();

        std::vector<std::vector<int>> edges;
        std::vector<int> edge_demands;
        int new_size = num_edges - removed_edges_in_order.size() - 1;
        edges.reserve(new_size);
        edge_demands.reserve(new_size);

        std::vector<bool> is_vertex_in_pivot(hypergraph.vertices.size(), false);
        for (const int vertex : hypergraph.edges[pivot_edge]) {
            is_vertex_in_pivot[vertex] = true;
        }

        int next_removed_edge_pos = 0;
        for (int edge = 0; edge < num_edges; ++edge) {
            if (next_removed_edge_pos < static_cast<int>(removed_edges_in_order.size()) &&
                removed_edges_in_order[next_removed_edge_pos] == edge) {
                ++next_removed_edge_pos;
                continue;
            }
            if (edge == pivot_edge) {
                continue;
            }

            auto& new_edge = edges.emplace_back();
            for (const int vertex : hypergraph.edges[edge]) {
                if (is_vertex_in_pivot[vertex]) {
                    new_edge.push_back(vertex);
                }
            }
            const int difference_size = hypergraph.edges[edge].size() - new_edge.size();
            edge_demands.push_back(std::max(0, hypergraph.edge_demands[edge] - difference_size));
        }

        return ConstructHypergraphFromEdges(edges, edge_demands);
    }
}
