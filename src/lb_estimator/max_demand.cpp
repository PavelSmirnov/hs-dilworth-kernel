#include "max_demand.h"
#include "common/log.h"
#include <algorithm>

namespace fastweihe {
    int MaxDemandLowerBoundEstimator::EstimateLowerBound(const Hypergraph& input, const int log_level) const {
        FASTWEIHE_INFO(log_level, "Estimating max demand lower bound.\n");
        const auto max_demand = std::max_element(input.edge_demands.begin(), input.edge_demands.end());
        if (max_demand == input.edge_demands.end()) {
            return 0;
        }
        return *max_demand;
    }
}
