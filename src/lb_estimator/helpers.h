#pragma once
#include "common/hypergraph.h"

namespace fastweihe {
    Hypergraph ConstructPushThroughHypergraph(const Hypergraph& hypergraph,
                                              const int pivot_edge,
                                              const std::vector<int>& removed_edges);
}
