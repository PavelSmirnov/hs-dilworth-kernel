#pragma once
#include "common/hypergraph.h"
#include <memory>
#include <string>

namespace fastweihe {
    class LowerBoundEstimator {
    public:
        virtual int EstimateLowerBound(const Hypergraph& input, const int log_level) const = 0;
        virtual ~LowerBoundEstimator();
    };

    std::unique_ptr<LowerBoundEstimator> MakeLowerBoundEstimator(const std::string& name);
}
