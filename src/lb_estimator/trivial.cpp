#include "trivial.h"
#include "common/log.h"

namespace fastweihe {
    int TrivialLowerBoundEstimator::EstimateLowerBound(const Hypergraph&, const int log_level) const {
        FASTWEIHE_INFO(log_level, "Estimating trivial lower bound.\n");
        return 0;
    }
}
