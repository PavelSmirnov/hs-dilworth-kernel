#include "hypergraph.h"

namespace fastweihe {
    bool IsHittingSetInstanceConsistent(const Hypergraph& instance) {
        for (int edge = 0; edge < static_cast<int>(instance.edges.size()); ++edge) {
            if (instance.edge_demands[edge] > static_cast<int>(instance.edges[edge].size())) {
                return false;
            }
        }
        return true;
    }

    bool CheckIsOptimalSolution(const Hypergraph& instance) {
        if (!IsHittingSetInstanceConsistent(instance)) {
            return false;
        }
        std::vector<bool> is_vertex_required(instance.vertices.size(), false);
        for (int edge = 0; edge < static_cast<int>(instance.edges.size()); ++edge) {
            if (static_cast<int>(instance.edges[edge].size()) != instance.edge_demands[edge]) {
                continue;
            }
            for (const auto vertex : instance.edges[edge]) {
                is_vertex_required[vertex] = true;
            }
        }
        for (int vertex = 0; vertex < static_cast<int>(instance.vertices.size()); ++vertex) {
            if (!is_vertex_required[vertex] && !instance.vertices[vertex].empty()) {
                return false;
            }
        }
        return true;
    }

    int GetNumUsedVertices(const Hypergraph& graph) {
        int num_used_vertices = 0;
        for (const auto& vertex_edges : graph.vertices) {
            num_used_vertices += !vertex_edges.empty();
        }
        return num_used_vertices;
    }
}
