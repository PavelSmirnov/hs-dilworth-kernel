#pragma once
#include <iostream>
#include <map>
#include <string>
#include <vector>

namespace fastweihe {
#define FASTWEIHE_LOG(log_level, required_level, message) {if (log_level >= required_level) { (std::cerr << message).flush(); } }
#define FASTWEIHE_ERROR(log_level, message) FASTWEIHE_LOG(log_level, 1, "ERROR: " << message)
#define FASTWEIHE_WARNING(log_level, message) FASTWEIHE_LOG(log_level, 2, "WARNING: " << message)
#define FASTWEIHE_INFO(log_level, message) FASTWEIHE_LOG(log_level, 3, message)
#define FASTWEIHE_DEBUG(log_level, message) FASTWEIHE_LOG(log_level, 4, message)

    void OutputLog(const std::string& log_level_name, const std::string& message_log_level, const std::string& message);
    int GetLogLevel(const std::string& log_level_name);
    std::vector<std::string> GetLogLevelNames();
}
