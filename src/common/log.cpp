#include "log.h"

namespace fastweihe {
    namespace {
        const std::map<std::string, int> LOG_LEVELS = {
            {"silent", 0},
            {"error", 1},
            {"warning", 2},
            {"info", 3},
            {"debug", 4}
        };
    }

    void OutputLog(const std::string& log_level_name, const std::string& message_log_level, const std::string& message) {
        const int log_level = GetLogLevel(log_level_name);
        if (message_log_level == "silent") {
            ;
        } else if (message_log_level == "error") {
            FASTWEIHE_ERROR(log_level, message);
        } else if (message_log_level == "warning") {
            FASTWEIHE_WARNING(log_level, message);
        } else if (message_log_level == "info") {
            FASTWEIHE_INFO(log_level, message);
        } else if (message_log_level == "debug") {
            FASTWEIHE_DEBUG(log_level, message);
        } else {
            FASTWEIHE_ERROR(log_level, "Trying to print message with unknown log level: " << message_log_level);
        }
    }

    int GetLogLevel(const std::string& log_level_name) {
        return LOG_LEVELS.at(log_level_name);
    }

    std::vector<std::string> GetLogLevelNames() {
        std::vector<std::string> names;
        for (const auto& [key, value] : LOG_LEVELS) {
            names.push_back(key);
        }
        return names;
    }
}
