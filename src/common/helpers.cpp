#include "helpers.h"
#include "hypergraph.h"
#include <cassert>

namespace fastweihe {
    namespace {
        std::vector<std::vector<int>> GetSortedElements(const std::vector<std::vector<int>>& sets, const int num_elements) {
            std::vector<std::vector<int>> elements(num_elements);
            for (int one_set = 0; one_set < static_cast<int>(sets.size()); ++one_set) {
                for (const int element : sets[one_set]) {
                    elements[element].push_back(one_set);
                }
            }
            return elements;
        }
    } // anonymous namespace

    Hypergraph SortHypergraphEdges(const Hypergraph& graph) {
        return {GetSortedElements(graph.vertices, graph.edges.size()),
                graph.vertices,
                graph.edge_demands};
    }

    Hypergraph SortHypergraphEdgesAndVertices(const Hypergraph& graph) {
        return {GetSortedElements(graph.vertices, graph.edges.size()),
                GetSortedElements(graph.edges, graph.vertices.size()),
                graph.edge_demands};
    }

    Hypergraph ConstructHypergraphFromEdges(const std::vector<std::vector<int>>& edges,
                                            const std::vector<int>& edge_demands) {
        Hypergraph result = Hypergraph{edges, {}, edge_demands};
        CompleteHypergraphWithVertices(&result);
        return result;
    }

    void CompleteHypergraphWithVertices(Hypergraph* graph) {
        assert(graph);

        graph->vertices.clear();
        for (int edge = 0; edge < static_cast<int>(graph->edges.size()); ++edge) {
            for (const int vertex : graph->edges[edge]) {
                if (vertex >= graph->vertices.size()) {
                    graph->vertices.resize(vertex + 1);
                }
                graph->vertices[vertex].push_back(edge);
            }
        }
    }

    Hypergraph RemoveEdgesFromHypergraph(const Hypergraph& hypergraph,
                                         const std::vector<int>& removed_edges_in_order) {
        std::vector<std::vector<int>> edges;
        std::vector<int> edge_demands;
        const int new_size = hypergraph.edges.size() - removed_edges_in_order.size();
        edges.reserve(new_size);
        edge_demands.reserve(new_size);

        int next_removed_edge_pos = 0;
        for (int edge = 0; edge < hypergraph.edges.size(); ++edge) {
            if (next_removed_edge_pos < removed_edges_in_order.size() &&
                removed_edges_in_order[next_removed_edge_pos] == edge) {
                ++next_removed_edge_pos;
                continue;
            }
            edges.push_back(hypergraph.edges[edge]);
            edge_demands.push_back(hypergraph.edge_demands[edge]);
        }

        return ConstructHypergraphFromEdges(edges, edge_demands);
    }
}
