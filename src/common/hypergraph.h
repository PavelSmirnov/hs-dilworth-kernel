#pragma once
#include <vector>

namespace fastweihe {
    struct Hypergraph {
        std::vector<std::vector<int>> edges;
        std::vector<std::vector<int>> vertices;
        std::vector<int> edge_demands;
    };

    bool IsHittingSetInstanceConsistent(const Hypergraph& instance);
    bool CheckIsOptimalSolution(const Hypergraph& instance);

    int GetNumUsedVertices(const Hypergraph&  graph);
}
