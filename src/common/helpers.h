#pragma once
#include "hypergraph.h"
#include <vector>

namespace fastweihe {
    Hypergraph SortHypergraphEdges(const Hypergraph& graph);
    Hypergraph SortHypergraphEdgesAndVertices(const Hypergraph& graph);

    Hypergraph ConstructHypergraphFromEdges(const std::vector<std::vector<int>>& edges,
                                            const std::vector<int>& edge_demands);
    void CompleteHypergraphWithVertices(Hypergraph* graph);

    Hypergraph RemoveEdgesFromHypergraph(const Hypergraph& hypergraph,
                                         const std::vector<int>& removed_edges_in_order);
}
