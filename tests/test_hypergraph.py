import os
import tempfile

import pytest

from fastweihe import HittingSetReducer, Hypergraph


WEIHE_ALGORITHMS = [algo for algo in HittingSetReducer.get_available_algorithms() if 'weihe' in algo]
F_WEIHE_ALGORITHMS = [algo for algo in HittingSetReducer.get_available_algorithms() if 'f-weihe' in algo]


HANDCRAFTED_HYPERGRAPHS = [
    # (edges, vertices, edge_demands)
    (
        [], [], []
    ), (
        [[0]], [[0]], [5]
    ), (
        [[0, 3, 6, 7], [2, 4, 5, 6, 7], [6]],
        [[0], [], [1], [0], [1], [1], [0, 1, 2], [0, 1]],
        [6, 7, 8]
    )
]


def _hypergraphs_equal(lhs, rhs):
    return lhs.edges == rhs.edges and lhs.vertices == rhs.vertices and lhs.edge_demands == rhs.edge_demands


@pytest.mark.parametrize('args', HANDCRAFTED_HYPERGRAPHS)
def test_create_from_edges(args):
    edges, vertices, edge_demands = args

    without_demands = Hypergraph(edges=edges)
    assert vertices == without_demands.vertices
    assert [1] * len(edges) == without_demands.edge_demands

    without_demands = Hypergraph(edges)
    assert vertices == without_demands.vertices
    assert [1] * len(edges) == without_demands.edge_demands

    with_demands = Hypergraph(edges=edges, edge_demands=edge_demands)
    assert vertices == with_demands.vertices
    assert edge_demands == with_demands.edge_demands

    with_demands = Hypergraph(edges, edge_demands)
    assert vertices == with_demands.vertices
    assert edge_demands == with_demands.edge_demands


@pytest.mark.parametrize('save_demands', [False, True])
def test_save_to_file(save_demands):
    edges = [[0, 3, 6, 7], [2, 4, 5, 6, 7], [6]]
    edge_demands = [6, 7, 8]
    graph = Hypergraph(edges=edges, edge_demands=edge_demands)
    if save_demands:
        text = """weighted
6 0 3 6 7
7 2 4 5 6 7
8 6"""
    else:
        text = """classic
0 3 6 7
2 4 5 6 7
6"""

    with tempfile.TemporaryDirectory() as tmpdir:
        path = os.path.join(tmpdir, 'graph.hyp')
        graph.save(path, save_demands=save_demands)
        with open(path) as f:
            assert text == f.read()


@pytest.mark.parametrize('test', HANDCRAFTED_HYPERGRAPHS)
def test_read_from_file(test):
    with tempfile.TemporaryDirectory() as tmpdir:
        edges, _, edge_demands = test
        without_demands = Hypergraph(edges=edges)
        with_demands = Hypergraph(edges=edges, edge_demands=edge_demands)

        without_demands_classic_path = os.path.join(tmpdir, 'without_demands_classic.hyp')
        without_demands.save(without_demands_classic_path, save_demands=False)
        without_demands_weighted_path = os.path.join(tmpdir, 'without_demands_weighted.hyp')
        without_demands.save(without_demands_weighted_path)
        with_demands_classic_path = os.path.join(tmpdir, 'with_demands_classic.hyp')
        with_demands.save(with_demands_classic_path, save_demands=False)
        with_demands_weighted_path = os.path.join(tmpdir, 'with_demands_weighted.hyp')
        with_demands.save(with_demands_weighted_path)

        assert _hypergraphs_equal(without_demands, Hypergraph(path=without_demands_classic_path))
        assert _hypergraphs_equal(without_demands, Hypergraph(path=without_demands_weighted_path))
        assert _hypergraphs_equal(without_demands, Hypergraph(path=with_demands_classic_path))
        assert _hypergraphs_equal(with_demands, Hypergraph(path=with_demands_weighted_path))


def test_no_vertex_duplicates():
    graph_with_duplicates = Hypergraph(edges=[[1, 2, 3], [1, 1, 1], [2, 2], [3, 3, 6, 3, 6]])
    edges = [[1, 2, 3], [1], [2], [3, 6]]
    assert edges == [list(sorted(edge)) for edge in graph_with_duplicates.edges]
