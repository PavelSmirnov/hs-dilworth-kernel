import itertools

import pytest

from fastweihe import LowerBoundEstimator, Hypergraph


EPS = 1e-6

def is_expected(output, expected_output):
    return expected_output - EPS <= output <= expected_output + EPS

LP_TESTS = [
    (
        Hypergraph(edges=[[0, 1, 2, 3], [2, 3, 4 ,5], [0, 1, 3, 5], [0, 2, 4, 5]], edge_demands=[1, 1, 1, 1]),
        4 / 3
    ),
    (
        Hypergraph(edges=[[0, 1, 2], [1, 2, 3], [2, 3, 4]], edge_demands=[2, 1, 2]),
        3
    ),
]

EXACT_TESTS = [
    (
        Hypergraph(edges=[[0, 1, 2, 3], [2, 3, 4 ,5], [0, 1, 3, 5], [0, 2, 4, 5]], edge_demands=[1, 1, 1, 1]),
        2
    ),
    (
        Hypergraph(edges=[[0, 1, 2], [1, 2, 3], [2, 3, 4]], edge_demands=[2, 1, 2]),
        3
    ),
]


@pytest.mark.parametrize('test', LP_TESTS)
def test_lp(test):
    input, expected_output = test
    estimator = LowerBoundEstimator(algorithm='lp')
    output = estimator(input)
    assert is_expected(output, expected_output)


@pytest.mark.parametrize('test', EXACT_TESTS)
def test_exact(test):
    input, expected_output = test
    estimator = LowerBoundEstimator(algorithm='exact')
    output = estimator(input)
    assert is_expected(output, expected_output)


@pytest.mark.parametrize('V_d_f', [(8, 4, 3), (10, 5, 3), (12, 4, 2)])
def test_regular(V_d_f):
    num_vertices, edge_size, demand = V_d_f
    h = Hypergraph(edges=[list(edge) for edge in itertools.combinations(range(num_vertices), edge_size)], edge_demands=demand)
    lp_lb = LowerBoundEstimator(algorithm='lp')(h)
    assert is_expected(lp_lb, num_vertices * demand / edge_size)
    exact_answer = LowerBoundEstimator(algorithm='exact')(h)
    assert is_expected(exact_answer, num_vertices - edge_size + demand)
