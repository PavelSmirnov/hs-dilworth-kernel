import pytest

from fastweihe import CyclingHittingSetReducer, HittingSetReducer, Hypergraph, StackingHittingSetReducer


FULL_SOLVERS = [algo for algo in HittingSetReducer.get_available_algorithms() if 'solve' in algo]
WEIHE_ALGORITHMS = [algo for algo in HittingSetReducer.get_available_algorithms() if 'weihe' in algo and 'f-weihe' not in algo]
F_WEIHE_ALGORITHMS = [algo for algo in HittingSetReducer.get_available_algorithms() if 'f-weihe' in algo and 'worse' not in algo]
ALL_ALGORITHMS = HittingSetReducer.get_available_algorithms()


WEIHE_HANDCRAFTED_TESTS = [
    (
        Hypergraph([]),
        Hypergraph([])
    ), (
        Hypergraph([[0]]),
        Hypergraph([[0]])
    ), (
        Hypergraph([[0, 1]]),
        Hypergraph([[0]])
    ), (
        Hypergraph([[0], [0]]),
        Hypergraph([[0]])
    ), (
        Hypergraph([[0, 1], [1]]),
        Hypergraph([[1]])
    ), (
        Hypergraph([[1, 2, 3], [4, 5 ,6], [5, 6, 7]]),
        Hypergraph([[1], [5]])
    ), (
        Hypergraph([[0, 1, 2], [1, 2, 3], [3, 4]]),
        Hypergraph([[1], [3]])
    ), (
        Hypergraph([[0], [0, 1], [1, 2], [2, 3], [3]]),
        Hypergraph([[0], [1], [3]])
    ), (
        Hypergraph([[0], [0, 1], [2]]),
        Hypergraph([[0], [2]])
    )
]

F_WEIHE_HANDCRAFTED_TESTS = [
    # EDGE REDUCTION
    (
        Hypergraph([[0, 1, 2, 3, 4, 5], [3, 4, 5, 6, 7, 8, 9], [0, 10], [1, 10], [2, 10], [3, 11], [4, 11], [5, 11], [6, 12], [7, 12], [8, 12], [9, 12], [6, 13], [7, 13], [8, 13], [9, 13]],
                   [5,                  2,                     1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1]),
        Hypergraph([[0, 1, 2, 3, 4, 5], [0, 10], [1, 10], [2, 10], [3, 11], [4, 11], [5, 11], [6, 12], [7, 12], [8, 12], [9, 12], [6, 13], [7, 13], [8, 13], [9, 13]],
                   [5,                  1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1])
    ),
    (
        Hypergraph([[0, 1, 2, 3, 4, 5], [3, 4, 5, 6, 7, 8, 9], [0, 10], [1, 10], [2, 10], [3, 11], [4, 11], [5, 11], [6, 12], [7, 12], [8, 12], [9, 12], [6, 13], [7, 13], [8, 13], [9, 13]],
                   [4,                  2,                     1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1]),
        Hypergraph([[0, 1, 2, 3, 4, 5], [3, 4, 5, 6, 7, 8, 9], [0, 10], [1, 10], [2, 10], [3, 11], [4, 11], [5, 11], [6, 12], [7, 12], [8, 12], [9, 12], [6, 13], [7, 13], [8, 13], [9, 13]],
                   [4,                  2,                     1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1])
    ),
    (
        Hypergraph([[0, 1, 2, 3, 4, 5], [3, 4, 5, 6, 7, 8, 9], [0, 10], [1, 10], [2, 10], [3, 11], [4, 11], [5, 11], [6, 12], [7, 12], [8, 12], [9, 12], [6, 13], [7, 13], [8, 13], [9, 13]],
                   [4,                  1,                     1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1]),
        Hypergraph([[0, 1, 2, 3, 4, 5], [0, 10], [1, 10], [2, 10], [3, 11], [4, 11], [5, 11], [6, 12], [7, 12], [8, 12], [9, 12], [6, 13], [7, 13], [8, 13], [9, 13]],
                   [4,                  1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1,       1])
    ),


    # VERTEX REDUCTION
    ( # irreducible, a flower with core [0, 1, 2, 3] and 3 petals, in each petal extra vertices are locked by vertex 15
        Hypergraph([[0, 1, 2, 3, 4, 5, 6], [0, 1, 2, 3, 7, 8, 9, 10], [0, 1, 2, 3, 11, 12, 13, 14], [4, 15], [5, 15], [6, 15], [7, 15], [8, 15], [9, 15], [10, 15], [11, 15], [12, 15], [13, 15], [14, 15]],
                   [4,                     4,                         4,                            1,       1,       1,       1,       1,       1,       1,        1,        1,        1,        1]),
        Hypergraph([[0, 1, 2, 3, 4, 5, 6], [0, 1, 2, 3, 7, 8, 9, 10], [0, 1, 2, 3, 11, 12, 13, 14], [4, 15], [5, 15], [6, 15], [7, 15], [8, 15], [9, 15], [10, 15], [11, 15], [12, 15], [13, 15], [14, 15]],
                   [4,                     4,                         4,                            1,       1,       1,       1,       1,       1,       1,        1,        1,        1,        1])
    ),
    ( # irreducible, previous graph with only one vertex petal a high demand
        Hypergraph([[0, 1, 2, 3, 4, 5, 6], [0, 1, 2, 3, 7, 8, 9, 10], [0, 1, 2, 3, 11, 12, 13, 14], [4, 15], [5, 15], [6, 15], [7, 15], [8, 15], [9, 15], [10, 15], [11, 15], [12, 15], [13, 15], [14, 15]],
                   [4,                     3,                         3,                            1,       1,       1,       1,       1,       1,       1,        1,        1,        1,        1]),
        Hypergraph([[0, 1, 2, 3, 4, 5, 6], [0, 1, 2, 3, 7, 8, 9, 10], [0, 1, 2, 3, 11, 12, 13, 14], [4, 15], [5, 15], [6, 15], [7, 15], [8, 15], [9, 15], [10, 15], [11, 15], [12, 15], [13, 15], [14, 15]],
                   [4,                     3,                         3,                            1,       1,       1,       1,       1,       1,       1,        1,        1,        1,        1])
    ),
    ( # all petals have sufficiently low demands to remove one vertex 3
        Hypergraph([[0, 1, 2, 3, 4, 5, 6], [0, 1, 2, 3, 7, 8, 9, 10], [0, 1, 2, 3, 11, 12, 13, 14], [4, 15], [5, 15], [6, 15], [7, 15], [8, 15], [9, 15], [10, 15], [11, 15], [12, 15], [13, 15], [14, 15]],
                   [3,                     3,                         3,                            1,       1,       1,       1,       1,       1,       1,        1,        1,        1,        1]),
        Hypergraph([[0, 1, 2, 4, 5, 6], [0, 1, 2, 7, 8, 9, 10], [0, 1, 2, 11, 12, 13, 14], [4, 15], [5, 15], [6, 15], [7, 15], [8, 15], [9, 15], [10, 15], [11, 15], [12, 15], [13, 15], [14, 15]],
                   [3,                  3,                      3,                         1,       1,       1,       1,       1,       1,       1,        1,        1,        1,        1])
    ),
    ( # demands are decreased again, the vertices 2 and 3 can be removed
        Hypergraph([[0, 1, 2, 3, 4, 5, 6], [0, 1, 2, 3, 7, 8, 9, 10], [0, 1, 2, 3, 11, 12, 13, 14], [4, 15], [5, 15], [6, 15], [7, 15], [8, 15], [9, 15], [10, 15], [11, 15], [12, 15], [13, 15], [14, 15]],
                   [2,                     2,                         2,                            1,       1,       1,       1,       1,       1,       1,        1,        1,        1,        1]),
        Hypergraph([[0, 1, 4, 5, 6], [0, 1, 7, 8, 9, 10], [0, 1, 11, 12, 13, 14], [4, 15], [5, 15], [6, 15], [7, 15], [8, 15], [9, 15], [10, 15], [11, 15], [12, 15], [13, 15], [14, 15]],
                   [2,               2,                   2,                      1,       1,       1,       1,       1,       1,       1,        1,        1,        1,        1])
    ),
    ( # now vertices 1, 2 and 3 are removed
        Hypergraph([[0, 1, 2, 3, 4, 5, 6], [0, 1, 2, 3, 7, 8, 9, 10], [0, 1, 2, 3, 11, 12, 13, 14], [4, 15], [5, 15], [6, 15], [7, 15], [8, 15], [9, 15], [10, 15], [11, 15], [12, 15], [13, 15], [14, 15]],
                   [1,                     1,                         1,                            1,       1,       1,       1,       1,       1,       1,        1,        1,        1,        1]),
        Hypergraph([[0, 4, 5, 6], [0, 7, 8, 9, 10], [0, 11, 12, 13, 14], [4, 15], [5, 15], [6, 15], [7, 15], [8, 15], [9, 15], [10, 15], [11, 15], [12, 15], [13, 15], [14, 15]],
                   [1,            1,                1,                   1,       1,       1,       1,       1,       1,       1,        1,        1,        1,        1])
    ),


    # OTHER TESTS
    (
        Hypergraph([[0, 1, 2]], [0]),
        Hypergraph([[]], [0])
    ),

    ( # irreducible
        Hypergraph([[0, 1, 2, 3, 4], [0, 1, 2, 5, 6], [2, 3, 4, 7, 8], [4, 9, 10], [5, 7, 9], [6, 8, 10]],
                   [2,               3,               3,               2,          1,         1]),
        Hypergraph([[0, 1, 2, 3, 4], [0, 1, 2, 5, 6], [2, 3, 4, 7, 8], [4, 9, 10], [5, 7, 9], [6, 8, 10]],
                   [2,               3,               3,               2,          1,         1])
    ),
    ( # the previous test with increased second edge demand
        Hypergraph([[0, 1, 2, 3, 4], [0, 1, 2, 5, 6], [2, 3, 4, 7, 8], [4, 9, 10], [5, 7, 9], [6, 8, 10]],
                   [2,               4,               3,               2,          1,         1]),
        Hypergraph([[0, 2, 5, 6], [2, 4, 7], [4, 9]],
                   [4,            3,         2])
    ),
    ( # with increased third edge demand
        Hypergraph([[0, 1, 2, 3, 4], [0, 1, 2, 5, 6], [2, 3, 4, 7, 8], [4, 9, 10], [5, 7, 9], [6, 8, 10]],
                   [2,               3,               4,               2,          1,         1]),
        Hypergraph([[2, 5, 6], [2, 4, 7, 8], [4, 9]],
                   [3,         4,            2])
    ),
    ( # with increased fourth edge demand
        Hypergraph([[0, 1, 2, 3, 4], [0, 1, 2, 5, 6], [2, 3, 4, 7, 8], [4, 9, 10], [5, 7, 9], [6, 8, 10]],
                   [2,               3,               3,               3,          1,         1]),
        Hypergraph([[0, 1, 2], [2, 3, 4], [4, 9, 10]],
                   [3,         3,         3])
    ),
]


@pytest.mark.parametrize('algorithm', WEIHE_ALGORITHMS + F_WEIHE_ALGORITHMS)
@pytest.mark.parametrize('test', WEIHE_HANDCRAFTED_TESTS)
def test_weihe_handcrafted(algorithm, test):
    reducer = HittingSetReducer(algorithm)
    input, expected_output = test
    output = reducer(input)
    assert expected_output == output
    assert (input != output) == reducer.is_reduced


def _generate_locked_chain_graph(length):
    edges = [[0, 1], [1, 2], [2, 0]]
    for i in range(length):
        edges.append([i + 2, i + 3])
    return Hypergraph(edges=edges)


@pytest.mark.parametrize('algorithm', WEIHE_ALGORITHMS + F_WEIHE_ALGORITHMS)
@pytest.mark.parametrize('length', [0, 1, 2, 3, 4, 5, 10, 100, 101])
def test_weihe_locked_chain(algorithm, length):
    input = _generate_locked_chain_graph(length)

    if length % 2 == 0:
        expected_edges = [[0, 1], [1, 2], [2, 0]]
        for i in range(1, length, 2):
            expected_edges.append([i + 2])
    else:
        expected_edges = [[0]]
        for i in range(0, length, 2):
            expected_edges.append([i + 2])
    expected_output = Hypergraph(edges=expected_edges)

    reducer = HittingSetReducer(algorithm)
    output = reducer(input)
    assert expected_output == output
    assert (input != output) == reducer.is_reduced


@pytest.mark.parametrize('algorithm', F_WEIHE_ALGORITHMS)
@pytest.mark.parametrize('test', F_WEIHE_HANDCRAFTED_TESTS)
def test_f_weihe_handcrafted(algorithm, test):
    reducer = HittingSetReducer(algorithm)
    input, expected_output = test
    output = reducer(input)
    assert expected_output == output
    assert (input != output) == reducer.is_reduced


def _check_edges_and_vertices_consistency(edges, vertices):
    for edge, edge_vertices in enumerate(edges):
        assert len(edge_vertices) == len(set(edge_vertices))
        for vertex in edge_vertices:
            assert vertex < len(vertices)
            assert edge in vertices[vertex]

    for vertex, vertex_edges in enumerate(vertices):
        assert len(vertex_edges) == len(set(vertex_edges))
        for edge in vertex_edges:
            assert edge < len(edges)
            assert vertex in edges[edge]

    assert not vertices or vertices[-1]


@pytest.mark.parametrize('algorithm', ALL_ALGORITHMS)
@pytest.mark.parametrize('test', WEIHE_HANDCRAFTED_TESTS)
def test_result_consistency(algorithm, test):
    reducer = HittingSetReducer(algorithm)
    input, _ = test
    output = reducer(input)

    edges = output.edges
    vertices = output.vertices
    _check_edges_and_vertices_consistency(output.edges, output.vertices)
    assert len(edges) == len(output.edge_demands)


@pytest.mark.parametrize('algorithm', WEIHE_ALGORITHMS)
def test_weihe_preserves_demands(algorithm):
    input = Hypergraph(edges=[[0, 1], [0], [2, 3], [2]], edge_demands=[1, 2, 3, 4])
    expected = Hypergraph(edges=[[0], [2]], edge_demands=[2, 4])
    reducer = HittingSetReducer(algorithm)
    output = reducer(input)
    assert expected == output


REMOVE_FULL_EDGES_TESTS = [
    (
        Hypergraph([]),
        Hypergraph([]),
        []
    ), (
        Hypergraph([[0]]),
        Hypergraph([]),
        [0]
    ), (
        Hypergraph([[0, 1]]),
        Hypergraph([[0, 1]]),
        []
    ), (
        Hypergraph([[0, 1, 2], [0, 1], [1, 2, 3], [1, 2]], edge_demands=[3, 2, 2, 2]),
        Hypergraph([[3]], edge_demands=[0]),
        [0, 1, 2]
    ), (
        Hypergraph([[0, 1, 2], [1, 2]], edge_demands=[3, 1]),
        Hypergraph([[]], edge_demands=[0]),
        [0, 1, 2]
    )
]


@pytest.mark.parametrize('test', REMOVE_FULL_EDGES_TESTS)
def test_remove_full_edges(test):
    input, expected_output, expected_in_hitting_set = test
    reducer = HittingSetReducer('remove-full-edges')
    output = reducer(input)
    assert expected_output == output
    assert expected_in_hitting_set == reducer.in_hitting_set
    assert (input != output) == reducer.is_reduced


@pytest.mark.parametrize('f_weihe_algo', F_WEIHE_ALGORITHMS)
@pytest.mark.parametrize('test', [
    (
        Hypergraph([]),
        Hypergraph([]),
        []
    ), (
        Hypergraph([[0, 1]], edge_demands=[2]),
        Hypergraph([]),
        [0, 1]
    ), (
        Hypergraph([[0, 1]]),
        Hypergraph([]),
        [0]
    ), (
        Hypergraph([[0], [0, 1, 2], [1, 2, 3, 4], [1, 5], [2, 5], [3, 5], [4, 5]], edge_demands=[1, 2, 3, 1, 1, 1, 1]),
        Hypergraph([[1, 2, 3, 4], [1, 5], [2, 5], [3, 5], [4, 5]], edge_demands=[3, 1, 1, 1, 1]),
        [0]
    )
    # TODO: add test with two remove-full-edges iterations
])
def test_cycling_reducer_with_f_weihe_and_remove_full_edges(f_weihe_algo, test):
    input, expected_output, expected_in_hitting_set = test
    reducer = CyclingHittingSetReducer(StackingHittingSetReducer([f_weihe_algo, 'remove-full-edges']))
    output = reducer(input)
    assert expected_output == output
    assert expected_in_hitting_set == reducer.in_hitting_set
    assert (input != output) == reducer.is_reduced


@pytest.mark.parametrize('weihe_algo', WEIHE_ALGORITHMS)
@pytest.mark.parametrize('test', [
    (
        Hypergraph([]),
        Hypergraph([]),
        []
    ), (
        Hypergraph([[0, 1]]),
        Hypergraph([]),
        [0]
    ), (
        Hypergraph([[1, 2]]),
        Hypergraph([]),
        [1]
    ), (
        Hypergraph([[1, 2], [2, 3], [1, 3]]),
        Hypergraph([[1, 2], [2, 3], [1, 3]]),
        []
    )
    # TODO: add test with two remove-full-edges iterations
])
def test_cycling_reducer_with_weihe_and_remove_full_edges(weihe_algo, test):
    input, expected_output, expected_in_hitting_set = test
    reducer = CyclingHittingSetReducer(StackingHittingSetReducer([weihe_algo, 'remove-full-edges']))
    output = reducer(input)
    assert expected_output == output
    assert expected_in_hitting_set == reducer.in_hitting_set
    assert (input != output) == reducer.is_reduced


WORSE_EDGE_RULE_TESTS = [
    (
        Hypergraph([]),
        Hypergraph([])
    ), (
        Hypergraph([[1, 2, 3], [1, 2, 3, 4, 5]], edge_demands=[2, 4]),
        Hypergraph([[1, 2, 3], [1, 2, 3, 4]], edge_demands=[2, 4])
    ), (
        Hypergraph([[1, 2, 3], [1, 2, 3, 4, 5]], edge_demands=[2, 2]),
        Hypergraph([[1, 2]], edge_demands=[2])
    )
]


@pytest.mark.parametrize('algo', ['f-weihe-naive-worse', 'f-weihe-fast-worse'])
@pytest.mark.parametrize('test', WORSE_EDGE_RULE_TESTS)
def test_worse_edge_rule(algo, test):
    input, expected_output = test
    reducer = HittingSetReducer(algo)
    output = reducer(input)
    assert expected_output == output


FULL_SOLUTION_TESTS = [
    (
        Hypergraph([]),
        0
    ), (
        Hypergraph([[]], [0]),
        0
    ), (
        Hypergraph([[1]]),
        1
    ), (
        Hypergraph([[0, 1, 2], [1, 2, 3]], [2, 2]),
        2
    ), (
        Hypergraph([[0, 1, 2], [2, 3, 4], [4, 5, 0]]),
        2
    )
]


class UnavailableAlgo(BaseException):
    pass


@pytest.mark.xfail(raises=UnavailableAlgo)
@pytest.mark.parametrize('algo', FULL_SOLVERS)
@pytest.mark.parametrize('test', FULL_SOLUTION_TESTS)
def test_full_solvers(algo, test):
    reducer = HittingSetReducer(algo)
    if not reducer.is_available:
        raise UnavailableAlgo
    input, expected_answer = test
    output = reducer(input)
    hitting_set = set(reducer.in_hitting_set)
    assert expected_answer == len(hitting_set)
    for edge, demand in zip(input.edges, input.edge_demands):
        assert demand <= len(hitting_set.intersection(edge))
